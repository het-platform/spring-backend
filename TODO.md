# To do

- [ ] Make price an optional parameter in the supermarket product details endpoint.
- [ ] Improve UT coverage:
  - ReceiptController
  - Exception handler (including ConstraintValidation)
  - DirkConnector
  - details-URLS (/products/{jumbo,ah}/{webshopId})
  - /product?barcode={barcode}
- [ ] Create more elegant, simplistic flow. (For instance, by reducing the amount of info returned to only what the endpoint is intended to do?)
- [ ] Make IT for getting a token from AH
- [X] Prevent UT from triggering real requests to APIs (ArgumentCaptor)
- [ ] Extract AH token logic into a separate class
- [ ] OpenFoodFactsConnectorIT
- [ ] Create endpoint to retrieve ecoscores of many barcodes simultaneously in parallel
- [X] URL-encode OFF-category links (Prio #1)
- [ ] Migrate to Agrybalyse categories
  - [ ] Find way to map OFF category -> Agribalyse category
- [ ] Improve OFF search using autosuggest URL: https://github.com/openfoodfacts/openfoodfacts-python/blob/develop/openfoodfacts/autosuggest.py
- [ ] Add improved Swagger documentation via annotations