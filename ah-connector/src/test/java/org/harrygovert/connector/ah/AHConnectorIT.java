package org.harrygovert.connector.ah;

import org.harrygovert.connector.ah.model.Category;
import org.harrygovert.connector.ah.model.Product;
import org.harrygovert.connector.ah.model.SearchResult;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.harrygovert.connector.ah.AHConnector.AHSearchSpecification;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class AHConnectorIT {
    private static final AHConnector CONNECTOR = new AHConnector();

    @Test
    void shouldSearchProductByName() {
        SearchResult response = CONNECTOR.searchProducts(AHSearchSpecification.builder().query("Nute").build());
        assertNotEquals(null, response);
        assertNotEquals(null, response.getProducts());
        assertNotEquals(0, response.getProducts().size());
    }

    @Test
    void shouldSearchAllProductsByName() {
        List<Product> response = CONNECTOR.searchAllProducts(AHSearchSpecification.builder().query("Nutella").build());
        assertNotEquals(null, response);
        assertNotEquals(0, response.size());
    }

    @Test
    void shouldGetProductDetailsById() {
        assertNotEquals(null, CONNECTOR.getProductDetails("453918"));
    }

    @Test
    void shouldGetProductDetailsByBarcode() {
        assertNotEquals(null, CONNECTOR.getProductByBarcode("8720181058028"));
    }

    @Test
    void shouldGetCategories() {
        List<Category> response = CONNECTOR.getCategories();
        assertNotEquals(null, response);
        assertNotEquals(0, response.size());
    }

    @Test
    void shouldGetSubCategoriesByCategoryId() {
        assertNotEquals(null, CONNECTOR.getSubCategories("6401"));
    }
}