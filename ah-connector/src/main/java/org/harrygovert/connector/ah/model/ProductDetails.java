package org.harrygovert.connector.ah.model;

import lombok.Data;

@Data
public class ProductDetails {

    private String productId;
    private Product productCard;
    private TradeItem tradeItem;
    private ProductProperties properties;
    private String disclaimerText;
}
