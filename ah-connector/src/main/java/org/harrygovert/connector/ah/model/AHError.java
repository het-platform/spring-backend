package org.harrygovert.connector.ah.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.harrygovert.connector.model.ApiError;

import java.time.ZonedDateTime;

@EqualsAndHashCode
@Data
public class AHError implements ApiError {

    private ZonedDateTime timestamp;
    private Integer status;
    private String error;
    @JsonAlias("error_description")
    private String errorDescription;
    private String message;
    private String path;
    private String correlationId;
}