package org.harrygovert.connector.ah.model;

import lombok.Data;

import java.util.List;

@Data
public class ProductLifespan {

    private List<ItemPeriod> itemPeriodSafeToUseAfterOpening;

}
