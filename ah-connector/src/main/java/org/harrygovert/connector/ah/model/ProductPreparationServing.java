package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class ProductPreparationServing {

    private List<AHPreparationServing> preparationServing;
    private AHServingQuantityInformation servingQuantityInformation;

    @Data
    @ToString
    public static class AHServingQuantityInformation {
        private Integer maximumNumberOfSmallestUnitsPerPackage;
        private Integer numberOfSmallestUnitsPerPackage;
        private Double numberOfServingsPerPackage;
    }

    @Data
    @ToString
    public static class AHPreparationServing {

        private String instructions;
        private String servingSuggestion;
    }
}
