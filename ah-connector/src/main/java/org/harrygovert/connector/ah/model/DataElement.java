package org.harrygovert.connector.ah.model;

import lombok.Data;

@Data
public class DataElement {

    private String value;
    private String label;
}
