package org.harrygovert.connector.ah;

import com.github.mizosoft.methanol.Methanol;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.harrygovert.connector.AbstractConnector;
import org.harrygovert.connector.ah.model.AHError;
import org.harrygovert.connector.ah.model.AnonymousTokenResponse;
import org.harrygovert.connector.ah.model.Category;
import org.harrygovert.connector.ah.model.Product;
import org.harrygovert.connector.ah.model.ProductDetails;
import org.harrygovert.connector.ah.model.SearchResult;
import org.harrygovert.connector.ah.model.SubCategoryDetails;
import org.harrygovert.connector.exception.ApiException;
import org.harrygovert.connector.model.ApiError;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.util.Arrays.asList;
import static java.util.concurrent.Executors.newScheduledThreadPool;
import static java.util.concurrent.TimeUnit.HOURS;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpHeaders.CONTENT_TYPE;
import static org.apache.http.HttpHeaders.USER_AGENT;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;
import static org.harrygovert.connector.util.HttpClientUtil.buildMethanolClient;
import static org.harrygovert.connector.util.HttpRequestUtil.get;
import static org.harrygovert.connector.util.HttpRequestUtil.post;

@Slf4j
public class AHConnector extends AbstractConnector {
    private static final String AH_USER_AGENT = "Appie/8.1.2 Model/phone Android/12-API31";
    private static final String AH_API_HOST = "api.ah.nl";
    private static final String AH_BASE_URL = format("https://%s", AH_API_HOST);
    private static final String AH_ANON_TOKEN_API_URL = format("%s/mobile-auth/v1/auth/token/anonymous", AH_BASE_URL);
    private static final String AH_ANON_TOKEN_REFRESH_API_URL = format("%s/mobile-auth/v1/auth/token/refresh", AH_BASE_URL);
    private static final String AH_SERVICES_URL = format("%s/mobile-services", AH_BASE_URL);
    private static final String AH_BARCODE_API_URL = format("%s/product/search/v1/gtin", AH_SERVICES_URL);
    private static final String AH_CATEGORY_API_URL = format("%s/v1/product-shelves/categories", AH_SERVICES_URL);
    private static final String AH_PRODUCT_API_URL = format("%s/product/detail/v4/fir", AH_SERVICES_URL);
    private static final String AH_SEARCH_API_URL = format("%s/product/search/v2", AH_SERVICES_URL);
    private static final ScheduledExecutorService TOKEN_REFRESH_SCHEDULER = newScheduledThreadPool(1);
    private String accessToken;
    private String refreshToken;

    public AHConnector() {
        this(DEFAULT_REQUEST_TIMEOUT_S);
    }

    public AHConnector(int timeoutSeconds) {
        this(buildMethanolClient(timeoutSeconds));
    }

    public AHConnector(Methanol methanolHttpClient) {
        super(methanolHttpClient);
        init();
    }

    public void init() {
        TOKEN_REFRESH_SCHEDULER.scheduleAtFixedRate(this::refreshAccessToken, 1, 1, HOURS);
    }

    private static String getTokenRequestPayload() {
        return "{\"clientId\": \"appie\"}";
    }

    private String getTokenRefreshPayload() {
        return "{\"clientId\": \"appie\", \"refreshToken\": \"" + refreshToken + "\"}";
    }

    private void setTokens(AnonymousTokenResponse response) {
        accessToken = "Bearer " + response.getAccessToken();
        refreshToken = response.getRefreshToken();
        log.info("Access token is now: {}", accessToken);
        log.info("Refresh token is now: {}", refreshToken);
    }

    private synchronized void setNewAccessToken() {
        log.info("Retrieving anonymous access token from AH");
        HttpRequest.Builder request = post(AH_ANON_TOKEN_API_URL, getTokenRequestPayload())
                .setHeader(CONTENT_TYPE, APPLICATION_JSON.toString());
        request = addDefaultAHHeaders(request);
        setTokens(performJsonRequest(request, AnonymousTokenResponse.class));
    }

    private synchronized void refreshAccessToken() {
        log.info("Refreshing anonymous access token from AH");
        HttpRequest.Builder request = post(AH_ANON_TOKEN_REFRESH_API_URL, getTokenRefreshPayload());
        request = addDefaultAHHeaders(request)
                .setHeader(CONTENT_TYPE, APPLICATION_JSON.toString());
        try {
            setTokens(performJsonRequest(request, AnonymousTokenResponse.class));
        } catch (ApiException e) {
            log.info("Refreshing failed - will attempt to get a new one ({})", e.getError());
            setNewAccessToken();
        }
    }

    private synchronized String getAccessToken() {
        if (accessToken == null) {
            setNewAccessToken();
        }
        return accessToken;
    }

    @Override
    protected Class<? extends ApiError> getErrorModelClass() {
        return AHError.class;
    }

    private static boolean errorIndicatesExpiredToken(ApiError error) {
        return Optional.ofNullable(error)
                .filter(AHError.class::isInstance)
                .map(AHError.class::cast)
                .map(AHError::getError)
                .map("invalid_token"::equals)
                .orElse(false);
    }

    public <T> T performJsonRequestWithToken(HttpRequest.Builder requestBuilder, Class<T> responseModelClass) {
        try {
            return performJsonRequest(requestBuilder, responseModelClass);
        } catch (ApiException e) {
            if (errorIndicatesExpiredToken(e.getError())) {
                log.info("Access token expired: {}", accessToken);
                refreshAccessToken();
                return performJsonRequest(requestBuilder, responseModelClass);
            }
            throw e;
        }
    }

    private HttpRequest.Builder createBaseAHGetRequestBuilder(URI uri) {
        return addDefaultAHHeaders(get(uri))
                .header(AUTHORIZATION, getAccessToken());
    }

    private HttpRequest.Builder createBaseAHGetRequestBuilder(String url) {
        return createBaseAHGetRequestBuilder(URI.create(url));
    }

    private HttpRequest.Builder addDefaultAHHeaders(HttpRequest.Builder builder) {
        return builder.header(USER_AGENT, AH_USER_AGENT)
                .header("X-Application", "AHWEBSHOP");
    }


    public Product getProductByBarcode(String barcode) {
        String url = format("%s/%s", AH_BARCODE_API_URL, barcode);
        HttpRequest.Builder request = createBaseAHGetRequestBuilder(url);
        return performJsonRequestWithToken(request, Product.class);
    }

    public List<Category> getCategories() {
        HttpRequest.Builder request = createBaseAHGetRequestBuilder(AH_CATEGORY_API_URL);
        return asList(performJsonRequestWithToken(request, Category[].class));
    }

    public SubCategoryDetails getSubCategories(String categoryId) {
        String url = format("%s/%s/sub-categories", AH_CATEGORY_API_URL, categoryId);
        HttpRequest.Builder request = createBaseAHGetRequestBuilder(url);
        return performJsonRequestWithToken(request, SubCategoryDetails.class);
    }

    @SneakyThrows
    public SearchResult searchProducts(AHSearchSpecification spec) {
        URI uri = new URIBuilder(AH_SEARCH_API_URL)
                .addParameter("sortOn", spec.getSort())
                .addParameter("page", valueOf(spec.getPage()))
                .addParameter("size", valueOf(spec.getSize()))
                .addParameter("query", spec.getQuery())
                .build();
        return performJsonRequestWithToken(createBaseAHGetRequestBuilder(uri), SearchResult.class);
    }

    public List<Product> searchAllProducts(AHSearchSpecification spec) {
        spec.setPage(0);
        SearchResult firstResult = searchProducts(spec);
        List<Product> allProducts = new ArrayList<>(firstResult.getProducts());
        for (int page = 1; page < firstResult.getPage().getTotalPages(); page++) {
            spec.setPage(page);
            allProducts.addAll(searchProducts(spec).getProducts());
        }
        return allProducts;
    }

    public ProductDetails getProductDetails(String webshopId) {
        String url = format("%s/%s", AH_PRODUCT_API_URL, webshopId);
        HttpRequest.Builder request = createBaseAHGetRequestBuilder(url);
        return performJsonRequestWithToken(request, ProductDetails.class);
    }

    @Builder
    @Data
    public static class AHSearchSpecification {

        @NonNull
        private final String query;
        @Builder.Default
        private int page = 0;
        @Builder.Default
        private int size = 10; // Maximum might be 750
        @Builder.Default
        private String sort = "RELEVANCE";
    }
}
