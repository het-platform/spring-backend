package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class SearchResult {
    @Data
    @ToString
    public static class AHPage {
        private Integer size;
        private Integer totalElements;
        private Integer totalPages;
        private Integer number;
    }

    private AHPage page;
    private List<Product> products;
    private Links links;
    private List<SearchFilter> filters;
    private List<String> sortOn;
    private SearchConfiguration configuration;
    private List<String> ads;
}
