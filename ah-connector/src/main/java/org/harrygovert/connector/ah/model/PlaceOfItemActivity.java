package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class PlaceOfItemActivity {

    private AHPlaceOfProductActivity placeOfProductActivity;

    @Data
    @ToString
    public static class AHPlaceOfProductActivity {

        private List<AHProductActivityDetails> productActivityDetails;
        private List<String> provenanceStatement;

        @Data
        @ToString
        public static class AHProductActivityDetails {

            private List<String> regionDescription;
            private DataElement typeCode;
        }
    }
}
