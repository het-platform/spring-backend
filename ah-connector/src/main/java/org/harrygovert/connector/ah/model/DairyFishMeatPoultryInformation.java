package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class DairyFishMeatPoultryInformation {

    private AHFishReportingInformation fishReportingInformation;

    @Data
    @ToString
    public static class AHFishReportingInformation {

        private AHCatchInformation catchInformation;

        @Data
        @ToString
        public static class AHCatchInformation {

            private List<DataElement> catchMethodCode;
        }
    }
}
