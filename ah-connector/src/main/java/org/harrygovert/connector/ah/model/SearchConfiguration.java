package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

@Data
public class SearchConfiguration {

    @Data
    @ToString
    private static class AHSearchConfigurationGoogleBanners {

        private String adUnitMainPath;
        private String adUnitSecondaryPath;
        private String customTemplateId;
        private String divGptAd;
    }

    private AHSearchConfigurationGoogleBanners googleBanners;
}