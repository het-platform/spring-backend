package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class TradeItem {

    private String gln;
    private String gtin;
    private List<AHAdditionalTradeItemIdentification> additionalTradeItemIdentification;
    private AHAlcoholInformation alcoholInformation;
    private List<AHAllergenInformationWrapper> allergenInformation;
    private AHAnimalFeeding animalFeeding;
    private List<CertificationInformationWrapper> certificationInformation;
    private AHConsumerInstructions consumerInstructions;
    private DairyFishMeatPoultryInformation dairyFishMeatPoultryInformation;
    private String foodAndBeverageIngredientStatement;
    private ProductPreparationServing foodAndBeveragePreparationServing;
    private HealthWellnessPackagingMarking healthWellnessPackagingMarking;
    private AHHealthRelatedInformation healthRelatedInformation;
    private AHHealthcareInformation healthcareInformation;
    private AHNutritionalInformationWrapper nutritionalInformation;
    private String nonfoodIngredientStatement;
    private AHPackagingMarking packagingMarking;
    private PharmaceuticalInformation pharmaceuticalInformation;
    private PlaceOfItemActivity placeOfItemActivity;
    private List<AHReferencedFileDetailInformation> referencedFileDetailInformation;
    private List<AHRegulatoryInformation> regulatoryInformation;
    private List<SafetyDatasheetInformation> safetyDataSheetInformation;
    private AHSalesInformation salesInformation;
    private List<ContactInformation> contactInformation;
    private AHProductDescription description;
    private ProductLifespan lifespan;
    private ProductMeasurements measurements;

    @Data
    @ToString
    public static class AHAnimalFeeding {

        private String additiveStatement;
        private String analyticalConstituentsStatement;
        private String compositionStatement;
        private List<DataElement> type;
        private String instructions;
        private List<DataElement> targetedConsumptionBy;
    }

    @Data
    @ToString
    public static class AHHealthcareInformation {

        private ItemPeriod itemMaximumUsageAge;
        private ItemPeriod itemMinimumUsageAge;
        private String itemUsageAgeDescription;
        private DataElement usageDuringBreastFeedingCode;
        private DataElement usageDuringPregnancyCode;
    }

    @Data
    @ToString
    public static class AHAlcoholInformation {

        private Double percentageOfAlcoholByVolume;
    }

    @Data
    @ToString
    public static class AHReferencedFileDetailInformation {

        private DataElement typeCode;
        private String uniformResourceIdentifier;
    }

    @Data
    @ToString
    public static class AHRegulatoryInformation {

        private List<String> permitIdentification;
        private List<DataElement> regulationTypeCode;
    }

    @Data
    @ToString
    public static class AHHealthRelatedInformation {

        private List<String> compulsoryAdditiveLabelInformation;
        private List<String> healthClaimDescription;
        private String sunProtectionFactor;
    }

    @Data
    @ToString
    public static class AHAllergenInformationWrapper {

        private List<AllergenInformation> items;
        private String statement;

    }

    @Data
    @ToString
    public static class AHConsumerInstructions {

        private List<String> storageInstructions;
        private List<String> usageInstructions;
    }

    @Data
    @ToString
    public static class AHNutritionalInformationWrapper {

        private List<NutrientHeader> nutrientHeaders;
        private List<String> nutritionalClaim;
    }

    @Data
    @ToString
    public static class AHPackagingMarking {

        private List<DataElement> localPackagingMarkedLabelAccreditationCodeReference;
        private List<DataElement> labelAccreditationCode;
    }

    @Data
    @ToString
    public static class AHProductDescription {

        private List<String> regulatedProductName;
    }

    @Data
    @ToString
    public static class AHAdditionalTradeItemIdentification {

        private String value;
        private DataElement typeCode;
    }

    @Data
    @ToString
    public static class AHSalesInformation {

        private List<DataElement> consumerSalesConditionCode;
    }
}
