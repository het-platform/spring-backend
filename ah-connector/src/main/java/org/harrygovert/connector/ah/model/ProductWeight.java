package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

@Data
public class ProductWeight {

    private AHProductDrainedWeight drainedWeight;

    @Data
    @ToString
    public static class AHProductDrainedWeight {

        private Double value;
        private DataElement measurementUnitCode;
        private Integer weightInGRM;
    }
}
