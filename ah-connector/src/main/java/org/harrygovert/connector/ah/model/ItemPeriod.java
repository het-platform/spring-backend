package org.harrygovert.connector.ah.model;

import lombok.Data;

@Data
public class ItemPeriod {

    private String value;
    private DataElement timeMeasurementUnitCode;
}
