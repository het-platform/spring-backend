package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

@Data
public class Links {
    @Data
    @ToString
    private static class AHLink {
        private String href;
    }

    private AHLink first;
    private AHLink current;
    private AHLink next;
    private AHLink last;
}
