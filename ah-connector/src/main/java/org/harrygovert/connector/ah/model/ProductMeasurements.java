package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class ProductMeasurements {

    private List<AHProductMeasurement> netContent;
    private String netContentStatement;
    private ProductWeight weight;

    @Data
    @ToString
    public static class AHProductMeasurement {

        private String value;
        private DataElement measurementUnitCode;
    }
}
