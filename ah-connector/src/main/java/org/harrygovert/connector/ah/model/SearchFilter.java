package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class SearchFilter {

    @Data
    @ToString
    private static class AHSearchFilterOption {

        private String id;
        private String label;
        private Integer count;
        private Boolean display;
    }

    private String id;
    private String label;
    private List<AHSearchFilterOption> options;
    private String type;
    private Boolean booleanFilter;
}
