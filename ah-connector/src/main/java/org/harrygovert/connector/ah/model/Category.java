package org.harrygovert.connector.ah.model;

import lombok.Data;
import org.harrygovert.connector.model.ProductImageDetails;

import java.util.List;

@Data
public class Category {
    private Integer id;
    private String name;
    private List<ProductImageDetails> images;
    private Boolean nix18;
}
