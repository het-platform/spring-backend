package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class NutrientHeader {

    private List<String> dailyValueIntakeReference;
    private AHNutrientQuantity nutrientBasisQuantity;
    private List<String> nutrientBasisQuantityDescription;
    private List<AHNutrientDetail> nutrientDetail;
    private DataElement preparationStateCode;

    @Data
    @ToString
    public static class AHNutrientQuantity {

        private Double value;
        private DataElement measurementUnitCode;
    }

    @Data
    @ToString
    public static class AHNutrientDetail {

        private Double dailyValueIntakePercent;
        private DataElement dailyValueIntakePercentMeasurementPrecisionCode;
        private DataElement measurementPrecisionCode;
        private List<String> nutrientSource;
        private DataElement nutrientTypeCode;
        private List<AHNutrientQuantity> quantityContained;
    }
}
