package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class CertificationInformationWrapper {

    private String agency;
    private List<AHCertificationInformation> items;

    @Data
    @ToString
    public static class AHCertificationInformation {

        private String value;
    }
}
