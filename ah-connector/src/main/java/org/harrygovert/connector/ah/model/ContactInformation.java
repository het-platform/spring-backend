package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class ContactInformation {

    private String contactAddress;
    private String contactName;
    private List<AHCommunicationChannelWrapper> targetMarketCommunicationChannel;
    private DataElement contactTypeCode;

    @Data
    @ToString
    public static class AHCommunicationChannelWrapper {

        private List<AHCommunicationChannel> communicationChannel;

        @Data
        @ToString
        public static class AHCommunicationChannel {
            private DataElement code;
            private String name;
            private String value;
        }
    }
}
