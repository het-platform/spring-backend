package org.harrygovert.connector.ah.model;

import lombok.Data;

import java.util.List;

@Data
public class HealthWellnessPackagingMarking {

    private List<DataElement> packagingMarkedDietAllergenCode;
    private List<DataElement> packagingMarkedFreeFromCode;
}
