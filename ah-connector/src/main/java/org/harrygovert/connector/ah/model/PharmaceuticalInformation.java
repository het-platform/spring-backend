package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class PharmaceuticalInformation {

    private List<AHDosage> dosages;
    private List<String> drugSideEffectsAndWarnings;

    @Data
    @ToString
    public static class AHDosage {

        private List<String> formTypeCodeReference;
        private List<String> recommendation;
    }
}
