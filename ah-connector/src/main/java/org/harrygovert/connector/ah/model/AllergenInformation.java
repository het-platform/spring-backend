package org.harrygovert.connector.ah.model;

import lombok.Data;

@Data
public class AllergenInformation {
    private DataElement typeCode;
    private DataElement levelOfContainmentCode;
}