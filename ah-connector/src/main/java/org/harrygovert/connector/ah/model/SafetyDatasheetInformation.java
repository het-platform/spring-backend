package org.harrygovert.connector.ah.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class SafetyDatasheetInformation {

    private AHGhsDetail ghsDetail;

    @Data
    @ToString
    public static class AHGhsDetail {

        private List<DataElement> ghsSymbolDescriptionCode;
        private List<AHSafetyStatement> hazardStatement;
        private List<AHSafetyStatement> precautionaryStatement;
        private DataElement ghsSignalWordsCode;

        @Data
        @ToString
        public static class AHSafetyStatement {
            private DataElement code;
            private List<String> description;
        }
    }
}
