package org.harrygovert.connector.ah.model;

import lombok.Data;

import java.util.List;

@Data
public class SubCategoryDetails {

    private Category parent;
    private List<Category> children;
}
