package org.harrygovert.connector.ah.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class AnonymousTokenResponse {
    @JsonAlias("access_token")
    private String accessToken;
    @JsonAlias("refresh_token")
    private String refreshToken;
}

