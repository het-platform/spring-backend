# Limitations

We have discovered the following functional limitations:

- Ecoscore data can only be obtained from OpenFoodFacts.
- The Jumbo and Dirk Product APIs do not return barcodes. The Albert Heijn API does, but only for the product details endpoint
