# Spring-BackEnd

## Running with Maven:
First, in the repository root: `mvn clean install`
Then, move to the backend-module: `cd harrygovert-backend`
Finally,
cmd: `mvn clean spring-boot:run -Dspring-boot.run.arguments="--server.ssl.enabled=false"`
powershell: `mvn clean spring-boot:run "-Dspring-boot.run.arguments=--server.ssl.enabled=false"`

## Running as jar:

`mvn clean package`
`cd harrygovert-backend`
`java -jar target/harrygovert-backend*.jar --server.ssl.enabled=false`

## After making changes, recompile the project and Spring will automatically reboot the app:

`mvn clean compile`

## Building and running the tests

`mvn clean install`

## Running/building in IntelliJ

shift+F10 and shift+F9 respectively

## Endpoints

- Search products by name across supermarkets: `/products/search?name=Banana`
- Get products from receipt by name and price per supermarket (currently only AH, as only AH can return barcodes which we need to retrieve EcoScore data):
  - `/receipts/search/ah?names=Banana&prices=1.45`
  - `/receipts/search/ah?names=Banana,Apple&prices=1.45,1.65`
- Get single product details per supermarket:
  - `/products/ah/<webshopId>`
  - `/products/jumbo/<webshopId>`
- Get product ecoscore from OpenFoodFacts by barcode: `/products/ecoscore/<barcode>`
- View Swagger UI: /swagger-ui.html
- View Swagger JSON: /v3/api-docs
- Download Swagger YAML: /v3/api-docs.yaml

## SSL Certificates

Creating a Certificate Signing Request (CSR):

```bash
openssl req -new -key private4096.key -out hgv-dev.csr \
    -subj '/C=NL/ST=Utrecht/L=Utrecht/O=Harry Go Vert/CN=harrygovert-app-dev.us-east-2.elasticbeanstalk.com' \
    -addext 'subjectAltName = DNS:localhost'
```

Self-signing a CSR (dev/testing only):

```bash
openssl x509 -req -signkey private4096.key -in hgv-dev.csr -days 365 -out hgv-dev.crt \
    -extfile <(printf 'subjectAltName = DNS:localhost')
```

Generating a keystore for an existing certificate:

```bash
openssl pkcs12 -export -in hgv-dev.crt -inkey private4096.key -name keyalias -password pass:<your_password> -out keystore.p12
```

Setting up NGINX for allowing HTTPS

- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/platforms-linux-extend.html
- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/java-se-nginx.html
