#!/bin/bash
# Run from project root, not from this folder
scripts/create_zip.sh
set -x
eb setenv -e ${EB_ENV:-harrygovert-app-dev} SERVER_PORT=5000 KEYSTORE_PASSWORD=$KEYSTORE_PASSWORD
eb deploy ${EB_ENV:-harrygovert-app-dev}