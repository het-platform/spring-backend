#!/bin/bash
# Run from project root, not from this folder
rm harrygovert-app-bundle.zip;
zip -j harrygovert-app-bundle.zip harrygovert-backend/target/app*jar;
zip -r harrygovert-app-bundle.zip .platform .ebextensions;