package org.harrygovert.connector;

import lombok.Setter;
import org.harrygovert.connector.model.ApiError;
import org.junit.jupiter.api.Test;

import static org.apache.http.HttpStatus.SC_OK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class AbstractConnectorTest {
    @Setter
    private static class DoubleContainer {
        private Double myDouble;
    }

    private final AbstractConnector testConnector = new AbstractConnector(null) {
        @Override
        protected Class<? extends ApiError> getErrorModelClass() {
            return null;
        }
    };

    @Test
    void shouldCorrectlyDeserializeDouble() {
        DoubleContainer result = testConnector.translateJsonResponse(SC_OK, "{\"myDouble\": 1.0}", DoubleContainer.class);
        assertNotEquals(null, result);
        assertEquals(1.0, result.myDouble);
    }

    @Test
    void shouldCorrectlyDeserializeDoubleWithComma() {
        DoubleContainer result = testConnector.translateJsonResponse(SC_OK, "{\"myDouble\": \"1,0\"}", DoubleContainer.class);
        assertNotEquals(null, result);
        assertEquals(1.0, result.myDouble);
    }

    @Test
    void shouldTurnInvalidDoubleInto0() {
        DoubleContainer result = testConnector.translateJsonResponse(SC_OK, "{\"myDouble\": \"nonsense\"}", DoubleContainer.class);
        assertNotEquals(null, result);
        assertEquals(0, result.myDouble);
    }

    @Test
    void shouldTurnEmptyDoubleIntoNull() {
        DoubleContainer result = testConnector.translateJsonResponse(SC_OK, "{\"myDouble\": \"\"}", DoubleContainer.class);
        assertNotEquals(null, result);
        assertNull(result.myDouble);
    }
}