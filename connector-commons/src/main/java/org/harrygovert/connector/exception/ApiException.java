package org.harrygovert.connector.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.harrygovert.connector.model.ApiError;

@Getter
@RequiredArgsConstructor
public class ApiException extends RuntimeException {

    private final ApiError error;
    private final String responsePayload;

    @Override
    public String getMessage() {
        return error.toString();
    }
}
