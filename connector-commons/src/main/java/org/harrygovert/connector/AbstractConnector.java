package org.harrygovert.connector;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.mizosoft.methanol.Methanol;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.harrygovert.connector.exception.ApiException;
import org.harrygovert.connector.model.ApiError;
import org.harrygovert.connector.model.GenericApiError;

import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.ZonedDateTime;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.http.HttpStatus.SC_OK;
import static org.harrygovert.connector.util.DeserializationUtil.createCommaInsensitiveDoubleDeserializer;
import static org.harrygovert.connector.util.DeserializationUtil.createLocalOrZonedDateTimeDeserializer;


@Slf4j
@RequiredArgsConstructor
public abstract class AbstractConnector {
    public static final int DEFAULT_REQUEST_TIMEOUT_S = 5;
    protected static final String FAKE_USER_AGENT = "Mozilla/5.0 (Macintosh; k; rv:81.0) Gecko/20100101 Firefox/81.0";
    private static final ObjectMapper OBJECT_MAPPER = createObjectMapper();
    private final Methanol methanolHttpClient;

    private static ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.registerModule(new SimpleModule()
                .addDeserializer(Double.class, createCommaInsensitiveDoubleDeserializer())
                .addDeserializer(ZonedDateTime.class, createLocalOrZonedDateTimeDeserializer()));
        objectMapper.configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    @SneakyThrows
    public <T> T performJsonRequest(HttpRequest.Builder requestBuilder, Class<T> responseModelClass) {
        var request = requestBuilder.build();
        log.info("Request URI: {}", request.uri());
        var response = methanolHttpClient.send(request, BodyHandlers.ofString());
        log.info("Response content: {}", response.body());
        return processResponse(responseModelClass, response);
    }

    private <T> T processResponse(Class<T> responseModelClass, HttpResponse<String> response) {
        int statusCode = response.statusCode();
        String responsePayload = response.body();
        if (isEmpty(responsePayload)) {
            var error = new GenericApiError("Unknown error (empty response)", statusCode);
            throw new ApiException(error, responsePayload);
        }
        log.info("Response status: {}", statusCode);
        log.debug("Response payload: {}", responsePayload);
        return translateJsonResponse(statusCode, responsePayload, responseModelClass);
    }

    @SneakyThrows
    public <T> T translateJsonResponse(int statusCode, String responsePayload, Class<T> responseModelClass) {
        if (statusCode != SC_OK) {
            ApiError error = OBJECT_MAPPER.readValue(responsePayload, getErrorModelClass());
            throw new ApiException(error, responsePayload);
        }
        return OBJECT_MAPPER.readValue(responsePayload, responseModelClass);
    }

    protected abstract Class<? extends ApiError> getErrorModelClass();
}
