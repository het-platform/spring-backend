package org.harrygovert.connector.util;

import lombok.NoArgsConstructor;

import java.util.Base64;
import java.util.Optional;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class Base64Util {
    private static final Base64.Encoder BASE64_ENCODER = Base64.getEncoder();

    public static String encodeAsBase64(String input) {
        return Optional.ofNullable(input)
                .map(String::getBytes)
                .map(BASE64_ENCODER::encode)
                .map(String::new)
                .orElse(null);
    }
}
