package org.harrygovert.connector.util;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@NoArgsConstructor(access = PRIVATE)
@Slf4j
public class DeserializationUtil {

    public static JsonDeserializer<Double> createCommaInsensitiveDoubleDeserializer() {
        return new JsonDeserializer<>() {
            @Override
            public Double deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
                String valueAsString = jsonParser.getValueAsString();
                if (isEmpty(valueAsString)) {
                    return null;
                }
                try {
                    return Double.valueOf(valueAsString.replaceAll(",", "."));
                } catch (NumberFormatException e) {
                    log.warn("Attempted to deserialize {} as Double and failed - returning 0", valueAsString);
                    return 0D;
                }
            }
        };
    }

    public static JsonDeserializer<ZonedDateTime> createLocalOrZonedDateTimeDeserializer() {
        return new JsonDeserializer<>() {
            @Override
            public ZonedDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
                String valueAsString = jsonParser.getValueAsString();
                if (isEmpty(valueAsString)) {
                    return null;
                }
                try {
                    return ZonedDateTime.parse(valueAsString);
                } catch (DateTimeParseException e) {
                    var localDateTime = LocalDateTime.parse(valueAsString);
                    return localDateTime.atZone(ZoneId.of("Europe/Paris"));
                }
            }
        };
    }
}
