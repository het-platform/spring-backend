package org.harrygovert.connector.util;

import lombok.NoArgsConstructor;

import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.http.HttpHeaders;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;
import static java.net.http.HttpRequest.BodyPublisher;
import static java.net.http.HttpRequest.BodyPublishers;
import static java.net.http.HttpRequest.Builder;
import static java.net.http.HttpRequest.newBuilder;
import static java.nio.charset.StandardCharsets.UTF_8;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class HttpRequestUtil {
    public static Builder get(URI uri) {
        return newBuilder(uri).GET();
    }

    public static Builder get(String url) {
        return get(URI.create(url));
    }

    public static Builder post(URI uri, String body) {
        return post(uri, BodyPublishers.ofString(body));
    }

    public static Builder post(String url, String body) {
        return post(URI.create(url), body);
    }

    public static Builder post(URI uri, BodyPublisher bodyPublisher) {
        return newBuilder(uri).POST(bodyPublisher);
    }

    public static Builder post(String url, BodyPublisher bodyPublisher) {
        return post(URI.create(url), bodyPublisher);
    }

    private static String escapeStringForHeader(String string) {
        return string.replace("\\", "\\\\").replace("\"", "\\\"");
    }

    public static HttpHeaders createPhotoFormHeaders(String name, String filename, String mediaType) {
        var disposition = format("form-data; name=\"%s\"", escapeStringForHeader(name));
        if (filename != null) {
            disposition += format("; filename=\"%s\"", escapeStringForHeader(filename));
        }
        return HttpHeaders.of(
                Map.of(
                        "Content-Disposition", List.of(disposition),
                        "Content-Type", List.of(mediaType)
                ),
                (n, v) -> true
        );
    }

    public static String urlEncode(String value) {
        return URLEncoder.encode(value, UTF_8);
    }

    public static String urlDecode(String value) {
        return URLDecoder.decode(value, UTF_8);
    }
}
