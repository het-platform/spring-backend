package org.harrygovert.connector.util;

import com.github.mizosoft.methanol.Methanol;
import lombok.NoArgsConstructor;

import java.net.http.HttpClient;

import static java.net.http.HttpClient.Version.HTTP_2;
import static java.time.Duration.ofSeconds;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class HttpClientUtil {
    public static HttpClient buildHttpClient(int timeoutSeconds) {
        return HttpClient.newBuilder()
                .connectTimeout(ofSeconds(timeoutSeconds))
                .version(HTTP_2)
                .build();
    }

    public static Methanol buildMethanolClient(HttpClient httpClient) {
        return Methanol.newBuilder(httpClient).build();
    }

    public static Methanol buildMethanolClient(int timeoutSeconds) {
        return buildMethanolClient(buildHttpClient(timeoutSeconds));
    }
}