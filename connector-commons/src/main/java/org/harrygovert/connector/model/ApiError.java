package org.harrygovert.connector.model;

public interface ApiError {
    String getMessage();
}