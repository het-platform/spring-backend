package org.harrygovert.connector.model;

import lombok.Data;

@Data
public class ProductImageDetails {

    private int width;
    private int height;
    private String url;
}
