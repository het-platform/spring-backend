package org.harrygovert.connector.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode
@Data
@AllArgsConstructor
public class GenericApiError implements ApiError {
    private String message;
    private Integer status;
}
