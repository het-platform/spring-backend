package org.harrygovert.connector.jumbo.model;

import lombok.Data;
import lombok.ToString;

@Data
public class ProductDetails {
    private ProductWrapper product;

    @Data
    @ToString
    public static class ProductWrapper {
        private Product data;
    }
}
