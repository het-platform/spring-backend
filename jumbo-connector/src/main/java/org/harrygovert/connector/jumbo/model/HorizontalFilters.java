package org.harrygovert.connector.jumbo.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class HorizontalFilters {

    private List<JumboHorizontalFilter> data;

    @Data
    @ToString
    public static class JumboHorizontalFilter {

        private String title;
        private String filters;
    }
}
