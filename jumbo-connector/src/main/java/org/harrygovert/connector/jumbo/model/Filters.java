package org.harrygovert.connector.jumbo.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class Filters {

    private List<JumboFilter> data;

    @Data
    @ToString
    public static class JumboFilter {

        private String title;
        private String type;
        private List<JumboFilterItem> items;
        private int visibleAmount;

        @Data
        @ToString
        public static class JumboFilterItem {

            private String title;
            private String filters;
            private int count;
            private boolean isCategory;

            public boolean isIsCategory() {
                return isCategory;
            }

            public void setIsCategory(boolean category) {
                isCategory = category;
            }
        }
    }
}
