package org.harrygovert.connector.jumbo.model;

import lombok.Data;
import lombok.ToString;

@Data
public class Prices {

    private JumboPrice price;
    private JumboPrice promotionalPrice;
    private JumboUnitPrice unitPrice;

    @Data
    @ToString
    public static class JumboPrice {

        private String currency;
        private int amount;
    }

    @Data
    @ToString
    public static class JumboUnitPrice {

        private String unit;
        private JumboPrice price;
    }
}
