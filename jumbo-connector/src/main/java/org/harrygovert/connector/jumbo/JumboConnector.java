package org.harrygovert.connector.jumbo;

import com.github.mizosoft.methanol.Methanol;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.http.client.utils.URIBuilder;
import org.harrygovert.connector.AbstractConnector;
import org.harrygovert.connector.jumbo.model.CategoryResult;
import org.harrygovert.connector.jumbo.model.CategoryResult.JumboCategories.JumboCategory;
import org.harrygovert.connector.jumbo.model.JumboError;
import org.harrygovert.connector.jumbo.model.Product;
import org.harrygovert.connector.jumbo.model.ProductDetails;
import org.harrygovert.connector.jumbo.model.Products;
import org.harrygovert.connector.jumbo.model.SearchResult;
import org.harrygovert.connector.model.ApiError;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static org.apache.http.HttpHeaders.USER_AGENT;
import static org.harrygovert.connector.util.HttpClientUtil.buildMethanolClient;

public class JumboConnector extends AbstractConnector {

    private static final String JUMBO_BASE_API_URL = "https://mobileapi.jumbo.com/v17";
    private static final String JUMBO_PRODUCT_API_URL = JUMBO_BASE_API_URL + "/products";
    private static final String JUMBO_SEARCH_API_URL = JUMBO_BASE_API_URL + "/search";
    private static final String JUMBO_CATEGORY_API_URL = JUMBO_BASE_API_URL + "/categories";

    public JumboConnector() {
        this(DEFAULT_REQUEST_TIMEOUT_S);
    }

    public JumboConnector(int timeoutSeconds) {
        this(buildMethanolClient(timeoutSeconds));
    }

    public JumboConnector(Methanol methanolHttpClient) {
        super(methanolHttpClient);
    }

    private HttpRequest.Builder createBaseJumboGetRequestBuilder(URI uri) {
        return HttpRequest.newBuilder()
                .GET()
                .setHeader(USER_AGENT, AbstractConnector.FAKE_USER_AGENT)
                .uri(uri);
    }

    private HttpRequest.Builder createBaseJumboGetRequestBuilder(String url) {
        return createBaseJumboGetRequestBuilder(URI.create(url));
    }

    @Override
    protected Class<? extends ApiError> getErrorModelClass() {
        return JumboError.class;
    }

    @SneakyThrows
    public Product getProductByBarcode(String barcode) {
        URI uri = new URIBuilder(JUMBO_SEARCH_API_URL).addParameter("q", barcode).build();
        HttpRequest.Builder request = createBaseJumboGetRequestBuilder(uri);
        SearchResult jumboBarcodeResponse = performJsonRequest(request, SearchResult.class);
        return Optional.of(jumboBarcodeResponse.getProducts())
                .map(Products::getData)
                .filter(list -> !list.isEmpty())
                .map(list -> list.get(0))
                .orElse(null);
    }

    public List<JumboCategory> getCategories() {
        return getSubCategories(null);
    }

    @SneakyThrows
    public List<JumboCategory> getSubCategories(String categoryId) {
        URIBuilder uriBuilder = new URIBuilder(JUMBO_CATEGORY_API_URL);
        if (categoryId != null) {
            uriBuilder.addParameter("id", categoryId);
        }
        HttpRequest.Builder request = createBaseJumboGetRequestBuilder(uriBuilder.build());
        return performJsonRequest(request, CategoryResult.class).getCategories().getData();
    }

    @SneakyThrows
    public SearchResult searchProducts(JumboSearchSpecification spec) {
        int size = spec.getSize();
        int page = spec.getPage();
        if ((page + 1) * size > 10000) {
            throw new IllegalArgumentException("Pagination limit on JumboConnector is 10.000");
        }
        // TODO: query parameter "filters" allows searching by category. This can be done by having a category ID as
        //  its parameter. Every category ID is a number 2^32 - 2^x. To include multiple categories, the filter parameter
        //  must be set to 2^32 - 2^x1 - 2^x2 - ... with every x corresponding to a category. To include all categories,
        //  set filter to 0.
        URI uri = new URIBuilder(JUMBO_SEARCH_API_URL)
                .addParameter("q", spec.getQuery())
                .addParameter("offset", page * size + "")
                .addParameter("limit", size + "")
                .build();
        return performJsonRequest(createBaseJumboGetRequestBuilder(uri), SearchResult.class);
    }

    public List<Product> searchAllProducts(JumboSearchSpecification spec) {
        spec.setPage(0);
        SearchResult firstResult = searchProducts(spec);
        List<Product> allProducts = firstResult.getProducts().getData();
        int nPages = (firstResult.getProducts().getTotal() / spec.size) + 1;
        for (int page = 1; page < nPages; page++) {
            spec.setPage(page);
            allProducts.addAll(searchProducts(spec).getProducts().getData());
        }
        return allProducts;
    }

    public ProductDetails getProductDetails(String webshopId) {
        HttpRequest.Builder request = createBaseJumboGetRequestBuilder(format("%s/%s", JUMBO_PRODUCT_API_URL, webshopId));
        return performJsonRequest(request, ProductDetails.class);
    }

    @Builder
    @Data
    public static class JumboSearchSpecification {

        @NonNull
        private final String query;
        @Builder.Default
        private int page = 0;
        @Builder.Default
        private int size = 10;
    }
}
