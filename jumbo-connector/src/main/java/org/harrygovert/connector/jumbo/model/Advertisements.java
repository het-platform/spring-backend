package org.harrygovert.connector.jumbo.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class Advertisements {

    private List<JumboAdvertisement> data;

    @Data
    @ToString
    public static class JumboAdvertisement {

        private int position;
        private String adUnitIdAndroid;
        private String adUnitIdiPhone;
        private String adUnitIdiPad;
    }
}
