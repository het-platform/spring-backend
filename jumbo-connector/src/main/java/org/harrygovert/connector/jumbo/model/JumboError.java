package org.harrygovert.connector.jumbo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.harrygovert.connector.model.ApiError;

@EqualsAndHashCode
@Data
public class JumboError implements ApiError {

    private int statusCode;
    private String error;
    private int code;
    private String message;
    private String url;
}