package org.harrygovert.connector.jumbo.model;

import lombok.Data;

@Data
public class SearchResult {

    private Products products;
    private Filters filters;
    private HorizontalFilters horizontalFilters;
    private SortOptions sortOptions;
    private ProductLists productLists;
    private Advertisements advertisements;
}
