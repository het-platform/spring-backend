package org.harrygovert.connector.jumbo.model;

import lombok.Data;

import java.util.List;

@Data
public class Products {

    private List<Product> data;
    private int total;
    private int offset;
}
