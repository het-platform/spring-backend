package org.harrygovert.connector.jumbo.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class ProductLists {

    private List<JumboProductList> data;

    @Data
    @ToString
    public static class JumboProductList {

        @JsonAlias("listID")
        private String listId;
        private String title;
        private int followers;
        private String author;
        private boolean verified;
        private String thumbnail;
    }
}
