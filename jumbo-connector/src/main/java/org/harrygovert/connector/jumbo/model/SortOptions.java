package org.harrygovert.connector.jumbo.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class SortOptions {

    private List<JumboSortOption> data;

    @Data
    @ToString
    public static class JumboSortOption {

        private String title;
        private String sort;
    }
}
