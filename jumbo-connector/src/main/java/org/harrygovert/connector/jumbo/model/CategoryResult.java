package org.harrygovert.connector.jumbo.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
public class CategoryResult {

    private JumboCategories categories;

    @Data
    @ToString
    public static class JumboCategories {

        private List<JumboCategory> data;

        @Data
        @ToString
        public static class JumboCategory {

            private String id;
            private String title;
            private int subCategoriesCount;
            private String imageUrl;
        }
    }
}
