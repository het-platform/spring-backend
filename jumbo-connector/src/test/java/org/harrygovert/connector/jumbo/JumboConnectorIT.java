package org.harrygovert.connector.jumbo;

import org.harrygovert.connector.jumbo.model.Product;
import org.harrygovert.connector.jumbo.model.SearchResult;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.harrygovert.connector.jumbo.JumboConnector.JumboSearchSpecification;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class JumboConnectorIT {
    private static final JumboConnector CONNECTOR = new JumboConnector();

    @Test
    void shouldSearchProductByName() {
        SearchResult response = CONNECTOR.searchProducts(JumboSearchSpecification.builder().query("Nute").build());
        assertNotEquals(null, response);
        assertNotEquals(null, response.getProducts());
        assertNotEquals(null, response.getProducts().getData());
        assertNotEquals(0, response.getProducts().getData().size());
    }

    @Test
    void shouldSearchAllProductsByName() {
        List<Product> response = CONNECTOR.searchAllProducts(JumboSearchSpecification.builder().query("Nute").build());
        assertNotEquals(null, response);
        assertNotEquals(0, response.size());
    }

    @Test
    void shouldGetProductDetailsByBarcode() {
        assertNotEquals(null, CONNECTOR.getProductByBarcode("8720181027758"));
    }

    @Test
    void shouldGetProductDetailsById() {
        assertNotEquals(null, CONNECTOR.getProductDetails("216435STK"));
    }

    @Test
    void shouldGetAllCategories() {
        assertNotEquals(null, CONNECTOR.getCategories());
    }

    @Test
    void shouldGetSubCategories() {
        assertNotEquals(null, CONNECTOR.getSubCategories("4294962923"));
    }
}