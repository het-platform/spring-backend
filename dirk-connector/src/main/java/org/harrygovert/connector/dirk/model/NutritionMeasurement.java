package org.harrygovert.connector.dirk.model;

import lombok.Data;

import java.util.List;

@Data
public class NutritionMeasurement {
    private String basisUnit;
    private String basisValue;
    private List<NutritionMeasurement> measurements;
    private String nutrientName;
    private String nutrientType;
    private String preparedUnit;
    private String preparedValue;
    private String referenceIntakePercentage;
    private String servingSizeUnit;
    private String servingSizeValue;
}
