package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class Allergen {
    private String allergenName;
    private String allergenType;
    private String levelOfContainment;
}
