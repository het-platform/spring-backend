package org.harrygovert.connector.dirk.model;

import lombok.Data;

import java.util.List;

@Data
public class OfferCategory {
    private String backgroundImage;
    private List<OfferCategory> categories;
    private String code;
    private String icon;
    private String name;
    private OfferCategory parent;
}
