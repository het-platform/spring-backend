package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class UsageInstruction {
    private String description;
}
