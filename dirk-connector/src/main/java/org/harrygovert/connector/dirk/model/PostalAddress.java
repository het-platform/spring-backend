package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class PostalAddress {
    private String organizationName;
    private String postOfficeBoxNumber;
}
