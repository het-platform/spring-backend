package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class ServingSuggestion {
    private String description;
}
