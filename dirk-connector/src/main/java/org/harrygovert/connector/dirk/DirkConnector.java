package org.harrygovert.connector.dirk;

import com.github.mizosoft.methanol.Methanol;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.http.client.utils.URIBuilder;
import org.harrygovert.connector.AbstractConnector;
import org.harrygovert.connector.dirk.model.ErrorResponse;
import org.harrygovert.connector.dirk.model.Product;
import org.harrygovert.connector.dirk.model.ProductCategory;
import org.harrygovert.connector.dirk.model.SearchResponse;
import org.harrygovert.connector.dirk.model.SearchResult;
import org.harrygovert.connector.model.ApiError;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpRequest;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.util.Arrays.asList;
import static org.harrygovert.connector.util.HttpClientUtil.buildMethanolClient;
import static org.harrygovert.connector.util.HttpRequestUtil.get;

@Component
public class DirkConnector extends AbstractConnector {

    private static final String DIRK_BASE_API_URL = "https://app-api.dirk.nl/v2";
    private static final String DIRK_PRODUCTS_API_URL = DIRK_BASE_API_URL + "/catalog/products";
    private static final String DIRK_CATEGORIES_API_URL = DIRK_PRODUCTS_API_URL + "/categories";
    private static final String DIRK_SEARCH_API_URL = DIRK_BASE_API_URL + "/catalog/search";
    private static final int BIGGEST_STORE_ID = 200;

    public DirkConnector() {
        this(DEFAULT_REQUEST_TIMEOUT_S);
    }

    public DirkConnector(int timeoutSeconds) {
        this(buildMethanolClient(timeoutSeconds));
    }

    public DirkConnector(Methanol methanolHttpClient) {
        super(methanolHttpClient);
    }

    private HttpRequest.Builder createBaseDirkGetRequestBuilder(URI uri) {
        return get(uri);
    }

    private HttpRequest.Builder createBaseDirkGetRequestBuilder(String url) {
        return createBaseDirkGetRequestBuilder(URI.create(url));
    }

    @Override
    protected Class<? extends ApiError> getErrorModelClass() {
        return ErrorResponse.class;
    }

    public Product getProductByBarcode(String barcode) {
        String url = format("%s/gtin:%s", DIRK_PRODUCTS_API_URL, barcode);
        HttpRequest.Builder request = createBaseDirkGetRequestBuilder(url);
        return performJsonRequest(request, Product.class);
    }

    @SneakyThrows
    public List<ProductCategory> getCategories(DirkCategorySearchSpecification spec) {
        URIBuilder uri = new URIBuilder(DIRK_CATEGORIES_API_URL)
                .addParameter("storeId", valueOf(spec.getStoreId()));
        addParamIfNotNull(uri, "priceDate", spec.getPriceDate());
        HttpRequest.Builder request = createBaseDirkGetRequestBuilder(uri.build());
        return asList(performJsonRequest(request, ProductCategory[].class));
    }

    @SneakyThrows
    public SearchResponse searchProducts(DirkSearchSpecification spec) {
        URIBuilder uri = new URIBuilder(DIRK_SEARCH_API_URL)
                .addParameter("text", spec.getText())
                .addParameter("storeId", valueOf(spec.getStoreId()));
        addParamIfNotNull(uri, "priceDate", spec.getPriceDate());
        addParamIfNotNull(uri, "offset", spec.getOffset());
        addParamIfNotNull(uri, "limit", spec.getLimit());
        addParamIfNotNull(uri, "desc", spec.getDesc());
        addParamIfNotNull(uri, "sort", spec.getSort());
        HttpRequest.Builder request = createBaseDirkGetRequestBuilder(uri.build());
        return performJsonRequest(request, SearchResponse.class);
    }

    public List<SearchResult> searchAllProducts(DirkSearchSpecification spec) {
        spec.setLimit(1000000);
        return searchProducts(spec).getResults();
    }

    private static void addParamIfNotNull(URIBuilder uriBuilder, String key, Object value) {
        Optional.ofNullable(value)
                .map(String::valueOf)
                .ifPresent(d -> uriBuilder.addParameter(key, d));
    }

    public Product getProductDetails(String productId) {
        String url = format("%s/%s", DIRK_PRODUCTS_API_URL, productId);
        HttpRequest.Builder request = createBaseDirkGetRequestBuilder(url);
        return performJsonRequest(request, Product.class);
    }

    @Builder
    @Data
    public static class DirkCategorySearchSpecification {
        @Builder.Default
        private int storeId = BIGGEST_STORE_ID;
        private ZonedDateTime priceDate;
    }

    @Builder
    @Data
    public static class DirkSearchSpecification {
        @NonNull
        private final String text;
        @Builder.Default
        private int storeId = BIGGEST_STORE_ID;
        private Integer offset;
        private Integer limit;
        private Boolean desc;
        private ZonedDateTime priceDate;
        private String sort;
    }
}
