package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class ProductPackaging {
    private Boolean customizable;
    private Integer itemCount;
    private String packageSize;
    private String packageUnit;
}
