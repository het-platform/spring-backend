package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class ProductCertification {
    private String description;
    private String icon;
    private String title;
}
