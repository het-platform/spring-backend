package org.harrygovert.connector.dirk.model;

import lombok.Data;

import java.util.List;

@Data
public class ProductPackagingLabel {
    private String alcoholPercentage;
    private String allergenStatement;
    private List<Allergen> allergens;
    private Double basisQuantity;
    private String basisQuantityUnit;
    private String certifiedName;
    private ContactInformation consumerContact;
    private Double estimatedQuantity;
    private String estimatedQuantityUnit;
    private List<ExplicitNotation> explicitNotations;
    private String healthClaim;
    private String ingredientStatement;
    private List<Ingredient> ingredients;
    private String marketingClaim;
    private List<NutritionMeasurement> measurements;
    private String nutritionClaim;
    private String nutritionStatement;
    private List<PreparationInstruction> preparationInstructions;
    private String servingSizeDescription;
    private List<ServingSuggestion> servingSuggestions;
    private Double servingsPerPackage;
    private List<StorageCondition> storageConditions;
    private List<UsageInstruction> usageInstructions;
}
