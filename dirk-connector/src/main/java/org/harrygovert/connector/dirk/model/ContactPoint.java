package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class ContactPoint {
    private String email;
    private String telephone;
    private String website;
}
