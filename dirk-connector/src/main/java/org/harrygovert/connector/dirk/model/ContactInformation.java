package org.harrygovert.connector.dirk.model;

import lombok.Data;

import java.util.List;

@Data
public class ContactInformation {
    private List<ContactPoint> contactPoints;
    private String email;
    private PostalAddress postalAddress;
    private String telephone;
    private String visit;
    private String website;
}
