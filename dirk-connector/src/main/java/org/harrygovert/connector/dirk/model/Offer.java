package org.harrygovert.connector.dirk.model;

import lombok.Data;

import java.util.List;

@Data
public class Offer {
    private List<OfferCategory> categories;
    private String currency;
    private String createdAt;
    private String descriptiveSize;
    private String discountPrice;
    private String discountType;
    private String imageUrl;
    private Integer internalQuantity;
    private String offerId;
    private String originalPrice;
    private String priceLabel;
    private List<Product> products;
    private String subTitle;
    private String title;
    private String userChosenPricingId;
    private String validFrom;
    private String validUntil;
}
