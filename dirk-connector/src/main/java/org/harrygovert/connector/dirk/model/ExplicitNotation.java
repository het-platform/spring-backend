package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class ExplicitNotation {
    private String code;
    private String description;
    private String id;
    private Integer index;
    private String text;
}
