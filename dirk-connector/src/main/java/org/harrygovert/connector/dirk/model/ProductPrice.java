package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class ProductPrice {
    private String currency;
    private Integer eligibleQuantityMaximum;
    private Integer eligibleQuantityMinimum;
    private String measuredPrice;
    private String measuredQuantity;
    private String measuredQuantityUnit;
    private String originalPrice;
    private String priceLabel;
    private String salePrice;
    private String validFrom;
    private String validUntil;
}
