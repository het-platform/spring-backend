package org.harrygovert.connector.dirk.model;

import lombok.Data;

import java.util.List;

@Data
public class Product {
    private String availability;
    private String availableFrom;
    private String availableUntil;
    private String baseUrlForImages;
    private String brandName;
    private List<ProductCategory> categories;
    private List<ProductCertification> certifications;
    private String createdAt;
    private String descriptiveSize;
    private String imageUrl;
    private Integer internalQuantity;
    private String name;
    private List<Offer> offers;
    private ProductPackaging packaging;
    private List<ProductPrice> prices;
    private String productId;
    private ProductPackagingLabel productPackagingLabel;
    private String restriction;
    private String sku;
    private String tradeChannel;
    private String userChosenPricingId;
    private String variantDescription;
}
