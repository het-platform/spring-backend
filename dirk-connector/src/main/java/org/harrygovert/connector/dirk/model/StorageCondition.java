package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class StorageCondition {
    private String description;
}
