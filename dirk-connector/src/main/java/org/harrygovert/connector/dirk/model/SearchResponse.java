package org.harrygovert.connector.dirk.model;

import lombok.Data;

import java.util.List;

@Data
public class SearchResponse {
    private List<SearchResult> results;
}
