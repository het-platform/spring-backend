package org.harrygovert.connector.dirk.model;

import lombok.Data;

import java.util.List;

@Data
public class ProductCategory {
    private String backgroundImage;
    private List<ProductCategory> categories;
    private String code;
    private String icon;
    private String name;
    private ProductCategory parent;
}