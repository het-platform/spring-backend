package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class PreparationInstruction {
    private String description;
}
