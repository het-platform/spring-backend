package org.harrygovert.connector.dirk.model;

import lombok.Data;
import org.harrygovert.connector.model.ApiError;

@Data
public class ErrorResponse implements ApiError {
    private String message;
    private String requestId;
    private String type;
}
