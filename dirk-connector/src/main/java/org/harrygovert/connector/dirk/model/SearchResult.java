package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class SearchResult {
    private Offer offer;
    private Product product;
    private Double score;
}
