package org.harrygovert.connector.dirk.model;

import lombok.Data;

@Data
public class Ingredient {
    private String description;
    private String name;
    private String percentage;
}
