package org.harrygovert.connector.dirk;

import org.harrygovert.connector.dirk.model.ProductCategory;
import org.harrygovert.connector.dirk.model.SearchResponse;
import org.harrygovert.connector.dirk.model.SearchResult;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.harrygovert.connector.dirk.DirkConnector.DirkCategorySearchSpecification;
import static org.harrygovert.connector.dirk.DirkConnector.DirkSearchSpecification;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class DirkConnectorIT {
    private static final DirkConnector CONNECTOR = new DirkConnector();

    @Test
    void shouldSearchProductByName() {
        SearchResponse response = CONNECTOR.searchProducts(DirkSearchSpecification.builder().text("Nutella").build());
        assertNotEquals(null, response);
        assertNotEquals(null, response.getResults());
        assertNotEquals(0, response.getResults().size());
    }

    @Test
    void shouldSearchAllProductsByName() {
        List<SearchResult> response = CONNECTOR.searchAllProducts(DirkSearchSpecification.builder().text("Nutella").build());
        assertNotEquals(null, response);
        assertNotEquals(0, response.size());
    }

    @Test
    void shouldGetProductDetailsById() {
        assertNotEquals(null, CONNECTOR.getProductDetails("44286"));
    }

    @Test
    void shouldGetProductDetailsByBarcode() {
        assertNotEquals(null, CONNECTOR.getProductByBarcode("8720181027758"));
    }

    @Test
    void shouldGetCategories() {
        List<ProductCategory> response = CONNECTOR.getCategories(DirkCategorySearchSpecification.builder().build());
        assertNotEquals(null, response);
        assertNotEquals(0, response.size());
    }
}