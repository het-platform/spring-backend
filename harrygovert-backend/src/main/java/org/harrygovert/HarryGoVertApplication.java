package org.harrygovert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class HarryGoVertApplication {
    public static void main(String[] args) {
        SpringApplication.run(HarryGoVertApplication.class, args);
    }
}
