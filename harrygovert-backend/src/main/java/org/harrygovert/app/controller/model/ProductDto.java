package org.harrygovert.app.controller.model;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductDto {
    private String barcode;
    private String detailsLink;
    private EcoscoreDto ecoscoreDto;
    private String ecoscoreLink;
    private String imageUrl;
    private String name;
    private Map<ProductPhotoTypeDto, String> openFoodFactsImages;
}
