package org.harrygovert.app.controller.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EcoscoreDto {
    private static final String[] GRADES = new String[]{"f", "e", "d", "c", "b", "a"};
    private static final int MAX_ECOSCORE = 100;
    private BonusMalus bonusMalus;
    private Double co2EquivalentPerKg;
    private ImpactDistribution co2ImpactDistribution;
    private ImpactDistribution environmentalFootprintDistribution;
    private String grade;
    private Integer score;
    private String source;
    private Double standardDeviation;

    public static String ecoscoreToGrade(int score) {
        int gradeIndex = (int) (score / (((double) MAX_ECOSCORE) / GRADES.length));
        gradeIndex = Math.max(0, gradeIndex);
        gradeIndex = Math.min(GRADES.length - 1, gradeIndex);
        return GRADES[gradeIndex];
    }

    @Data
    @Builder
    @NoArgsConstructor(access = AccessLevel.PUBLIC)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class BonusMalus {
        private OriginsOfIngredientsBonusMalus originsOfIngredients;
        private Integer packaging;
        private Integer productionSystem;
        private Integer threatenedSpecies;
    }

    @Data
    @Builder
    @NoArgsConstructor(access = AccessLevel.PUBLIC)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class OriginsOfIngredientsBonusMalus {
        private Integer combined;
        private Integer environmentalPerformanceIndex;
        private Integer transportation;
    }

    @Data
    @Builder
    @NoArgsConstructor(access = AccessLevel.PUBLIC)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class ImpactDistribution {
        private Double agriculture;
        private Double consumption;
        private Double distribution;
        private Double packaging;
        private Double processing;
        private Double transportation;
    }
}
