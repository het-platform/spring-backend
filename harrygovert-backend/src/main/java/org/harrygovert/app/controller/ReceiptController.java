package org.harrygovert.app.controller;

import lombok.RequiredArgsConstructor;
import org.harrygovert.app.controller.model.AHReceiptSearchResult;
import org.harrygovert.app.service.AHProductService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@Validated
public class ReceiptController {
    private final AHProductService ahProductService;

    @GetMapping(path = "/receipts/ah/search")
    public List<AHReceiptSearchResult> searchCandidatesForReceiptProducts(@NotEmpty String[] names, @NotEmpty Double[] prices) {
        if (names.length != prices.length) {
            throw new IllegalArgumentException("Number of names must equal the number of prices!");
        }
        List<AHReceiptSearchResult> result = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            result.add(ahProductService.searchReceiptCandidates(names[i], prices[i]));
        }
        return result;
    }
}
