package org.harrygovert.app.controller.model;

public enum ProductPhotoTypeDto {
    front, ingredients, nutrition, packaging
}
