package org.harrygovert.app.controller;

import lombok.RequiredArgsConstructor;
import org.harrygovert.app.controller.exception.EcoscoreNotFoundException;
import org.harrygovert.app.controller.exception.ProductNotFoundException;
import org.harrygovert.app.controller.model.EcoscoreDto;
import org.harrygovert.app.controller.model.ProductDto;
import org.harrygovert.app.controller.model.ProductPhotoTypeDto;
import org.harrygovert.app.service.AHProductService;
import org.harrygovert.app.service.JumboProductService;
import org.harrygovert.app.service.OpenFoodFactsProductService;
import org.harrygovert.app.service.ProductByBarcodeService;
import org.harrygovert.app.service.ProductService;
import org.harrygovert.connector.jumbo.model.CategoryResult;
import org.harrygovert.connector.openfoodfacts.exception.ProductNotFoundInOpenFoodFactsException;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;
import java.util.List;

import static java.lang.Boolean.TRUE;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.harrygovert.connector.util.HttpRequestUtil.urlDecode;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@Validated
public class ProductController {
    public static final String SUCCESS_RESPONSE = "Request Successful";
    public static final String USE_OFF_TEST_SERVER_HEADER = "X-UseOffTestServer";
    private final OpenFoodFactsProductService openFoodFactsProductService;
    // Currently only contains AHProductService, because AH is the only API that returns barcodes.
    private final List<ProductService> productServices;
    private final AHProductService ahProductService;
    private final JumboProductService jumboProductService;
    private final ProductByBarcodeService productByBarcodeService;

    @PostMapping(path = "/products/{barcode}", produces = MediaType.TEXT_PLAIN_VALUE)
    public String addNewProduct(@PathVariable @NotNull @Pattern(regexp = "\\d{0,30}") String barcode,
                                @RequestParam(required = false) @Pattern(regexp = "[a-zA-Z0-9'\"() ]+") String name,
                                @RequestHeader(value = USE_OFF_TEST_SERVER_HEADER, required = false) Boolean useOffTestServer) {
        openFoodFactsProductService.addProductByBarcodeAndName(barcode, name, TRUE.equals(useOffTestServer));
        return SUCCESS_RESPONSE;
    }

    @PostMapping(path = "/products/{barcode}/photos", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public String addPhotoToProduct(@PathVariable @NotNull @Pattern(regexp = "\\d{0,30}") String barcode,
                                    @RequestParam("photo_type") ProductPhotoTypeDto photoType,
                                    @RequestParam("photo") MultipartFile photo,
                                    @RequestHeader(value = USE_OFF_TEST_SERVER_HEADER, required = false) Boolean useOffTestServer) {
        openFoodFactsProductService.addPhotoToProduct(barcode, photoType, photo, TRUE.equals(useOffTestServer));
        return SUCCESS_RESPONSE;
    }

    @Deprecated
    @GetMapping(path = "/products/ecoscore/{barcode}")
    public ProductDto getProductDetailsByBarcode(@PathVariable @NotNull String barcode) {
        try {
            return openFoodFactsProductService.getProductByBarcode(barcode);
        } catch (ProductNotFoundInOpenFoodFactsException ex) {
            throw new ProductNotFoundException(barcode);
        }
    }

    @GetMapping(path = "/products")
    public ProductDto getProductByBarcode(@RequestParam @NotNull String barcode) {
        return productByBarcodeService.getProductByBarcode(barcode);
    }

    @GetMapping(path = "/products/ecoscore")
    public EcoscoreDto getEcoscoreByBarcode(
            @RequestParam(required = false) String barcode,
            @RequestParam(required = false) String offCategory) {
        if (isNotBlank(barcode)) {
            return openFoodFactsProductService.getEcoscoreDataForBarcode(barcode);
        }
        if (isNotBlank(offCategory)) {
            String decodedOffCategory = urlDecode(offCategory);
            return openFoodFactsProductService.getAverageEcoscoreForCategory(decodedOffCategory)
                    .orElseThrow(() -> new EcoscoreNotFoundException("Could not get ecoscore for category " + decodedOffCategory));
        }
        throw new IllegalArgumentException("At least one of the (barcode and offCategory) parameters should be passed");
    }

    @GetMapping(path = "/products/ah/{webshopId}")
    public ProductDto getAhProductDetails(@PathVariable @NotNull String webshopId) {
        return ahProductService.getProductDetails(webshopId);
    }

    @GetMapping(path = "/products/jumbo/{webshopId}")
    public ProductDto getJumboProductDetails(@PathVariable @NotNull String webshopId) {
        return jumboProductService.getProductDetails(webshopId);
    }

    @GetMapping(path = "/products/search")
    public List<ProductDto> searchProduct(@RequestParam @NotNull String name) {
        // Only AH returns barcodes, making the other supermarket APIs kinda useless for searching :(
        return productServices.stream()
                .parallel()
                .map(service -> service.searchProductsAndIgnoreFailure(name))
                .flatMap(Collection::stream)
                .collect(toList());
    }

    // Temporary for testing
    @Deprecated
    @GetMapping(path = "/products/ah/search")
    public List<ProductDto> searchProductAh(@RequestParam @NotNull String name) {
        return ahProductService.searchProducts(name);
    }

    @Deprecated
    @GetMapping(path = "/products/jumbo/search")
    public List<ProductDto> searchProductJumbo(@RequestParam @NotNull String name) {
        return jumboProductService.searchProducts(name);
    }

    @Deprecated
    @GetMapping(path = "/categories/jumbo")
    public List<CategoryResult.JumboCategories.JumboCategory> searchCategoriesJumbo() {
        return jumboProductService.getCategories();
    }
}
