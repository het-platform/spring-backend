package org.harrygovert.app.controller.exception;

public class EcoscoreNotFoundException extends RuntimeException {
    public EcoscoreNotFoundException(String message) {
        super(message);
    }
}
