package org.harrygovert.app.controller.exception.handler;

import com.fasterxml.jackson.core.JsonParseException;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.harrygovert.app.controller.exception.EcoscoreNotFoundException;
import org.harrygovert.app.controller.exception.ProductNotFoundException;
import org.harrygovert.app.controller.model.ErrorResponse;
import org.harrygovert.connector.exception.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NOT_IMPLEMENTED;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(ProductNotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleProductNotFoundException(ProductNotFoundException ex) {
        return makeErrorResponseEntity(ex, NOT_FOUND);
    }

    @ExceptionHandler(EcoscoreNotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleEcoscoreNotFoundException(EcoscoreNotFoundException ex) {
        return makeErrorResponseEntity(ex, NOT_FOUND);
    }

    @ExceptionHandler(JsonParseException.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorResponse> handleJsonParseException(JsonParseException ex) {
        return makeErrorResponseEntity(ex, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotImplementedException.class)
    @ResponseStatus(NOT_IMPLEMENTED)
    public ResponseEntity<ErrorResponse> handleNotImplementedException(NotImplementedException ex) {
        return makeErrorResponseEntity(ex, NOT_IMPLEMENTED);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(BAD_REQUEST)
    public ResponseEntity<ErrorResponse> handleIllegalArgumentException(IllegalArgumentException ex) {
        return makeErrorResponseEntity(ex, BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(BAD_REQUEST)
    public ResponseEntity<ErrorResponse> handleConstraintViolationException(ConstraintViolationException ex) {
        return makeErrorResponseEntity(ex, BAD_REQUEST);
    }

    @ExceptionHandler(ApiException.class)
    @ResponseStatus(BAD_REQUEST)
    public ResponseEntity<ErrorResponse> handleProductPhotoAdditionFailedException(ApiException ex) {
        return makeErrorResponseEntity(ex, BAD_REQUEST);
    }

    @ExceptionHandler(FileSizeLimitExceededException.class)
    @ResponseStatus(BAD_REQUEST)
    public ResponseEntity<ErrorResponse> handleFileSizeLimitExceededException(FileSizeLimitExceededException ex) {
        return makeErrorResponseEntity(ex, BAD_REQUEST);
    }

    private static ResponseEntity<ErrorResponse> makeErrorResponseEntity(Exception ex, HttpStatus status) {
        return new ResponseEntity<>(makeErrorResponse(ex.getMessage()), status);
    }

    private static ErrorResponse makeErrorResponse(String message) {
        return ErrorResponse.builder().message(message).build();
    }
}
