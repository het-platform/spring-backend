package org.harrygovert.app.config;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class JumboMappings {
    private final Map<String, List<String>> jumboProductsToJumboCategories;
    private final Map<String, Map<String, String>> jumboCategoriesToOffCategories;

    public Optional<String> getJumboCategoryForJumboProductId(String productId) {
        return Optional.ofNullable(jumboProductsToJumboCategories.get(productId))
                .map(CollectionUtils::lastElement);
    }

    public Optional<String> getOffCategoryForJumboCategory(String categoryId) {
        return Optional.ofNullable(jumboCategoriesToOffCategories.get(categoryId))
                .map(cat -> cat.get("off_category"));
    }
}
