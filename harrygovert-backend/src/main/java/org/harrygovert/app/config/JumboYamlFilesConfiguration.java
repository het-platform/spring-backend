package org.harrygovert.app.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.yaml.snakeyaml.Yaml;

import java.util.List;
import java.util.Map;

import static org.harrygovert.util.FileUtil.getFile;

@Configuration
public class JumboYamlFilesConfiguration {
    private static final String JUMBO_PRODUCTS_TO_CATEGORIES_FILE = "jumbo_products_to_categories.yaml";
    private static final String JUMBO_CATEGORIES_TO_OFF_CATEGORIES_FILE = "jumbo_categories_to_off_categories_flattened.yaml";

    @Bean
    public Map<String, List<String>> jumboProductsToJumboCategories() {
        return new Yaml().load(getFile(JUMBO_PRODUCTS_TO_CATEGORIES_FILE));
    }

    @Bean
    public Map<String, Map<String, String>> jumboCategoriesToOffCategories() {
        return new Yaml().load(getFile(JUMBO_CATEGORIES_TO_OFF_CATEGORIES_FILE));
    }
}
