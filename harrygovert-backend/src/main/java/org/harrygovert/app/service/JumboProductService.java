package org.harrygovert.app.service;

import lombok.RequiredArgsConstructor;
import org.harrygovert.app.controller.model.ProductDto;
import org.harrygovert.connector.jumbo.JumboConnector;
import org.harrygovert.connector.jumbo.converter.JumboProductDetailsToProductDtoConverter;
import org.harrygovert.connector.jumbo.converter.JumboProductToProductDtoConverter;
import org.harrygovert.connector.jumbo.model.CategoryResult;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.harrygovert.connector.jumbo.JumboConnector.JumboSearchSpecification;

@Component
@RequiredArgsConstructor
public class JumboProductService extends ProductService {
    private final JumboConnector jumboConnector;
    private final JumboProductToProductDtoConverter productConverter;
    private final JumboProductDetailsToProductDtoConverter productDetailsConverter;

    @Override
    public ProductDto getProductByBarcode(String barcode) {
        return productConverter.convert(jumboConnector.getProductByBarcode(barcode), barcode);
    }

    @Override
    public List<ProductDto> searchProducts(String name) {
        JumboSearchSpecification spec = JumboSearchSpecification.builder()
                .query(name)
                .build();
        return jumboConnector.searchProducts(spec).getProducts().getData().stream()
                .map(productConverter::convert)
                .collect(toList());
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return searchProducts("");
    }

    public ProductDto getProductDetails(String webshopId) {
        return productDetailsConverter.convert(jumboConnector.getProductDetails(webshopId));
    }


    @Deprecated
    public List<CategoryResult.JumboCategories.JumboCategory> getCategories() {
        return jumboConnector.getCategories();
    }
}
