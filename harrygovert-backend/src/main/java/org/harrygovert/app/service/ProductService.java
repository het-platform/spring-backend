package org.harrygovert.app.service;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.harrygovert.app.controller.model.ProductDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

@Slf4j
public abstract class ProductService {
    public ProductDto getProductByBarcode(String barcode) {
        throw new NotImplementedException();
    }

    public List<ProductDto> getProductByBarcode(@NonNull List<String> barcodes) {
        return barcodes.stream()
                .map(this::getProductByBarcode)
                .collect(Collectors.toList());
    }

    public List<ProductDto> getAllProducts() {
        throw new NotImplementedException();
    }

    public List<ProductDto> searchProducts(String name) {
        throw new NotImplementedException();
    }

    public Optional<ProductDto> getProductByBarcodeAndIgnoreFailure(String barcode) {
        try {
            return ofNullable(getProductByBarcode(barcode));
        } catch (Exception e) {
            log.warn("Error while getting product by barcode: {} with message {}", e.getClass().getName(), e.getMessage());
            return empty();
        }
    }

    public List<ProductDto> searchProductsAndIgnoreFailure(String name) {
        try {
            return searchProducts(name);
        } catch (Exception e) {
            log.warn("Error while searching products: {} with message {}", e.getClass().getName(), e.getMessage());
            return new ArrayList<>();
        }
    }

    public List<ProductDto> searchProductsByCategory(String category) {
        throw new NotImplementedException();
    }
}
