package org.harrygovert.app.service;

import lombok.RequiredArgsConstructor;
import org.harrygovert.app.controller.exception.ProductNotFoundException;
import org.harrygovert.app.controller.model.ProductDto;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.springframework.util.CollectionUtils.firstElement;

@Component
@RequiredArgsConstructor
public class ProductByBarcodeService {
    private final AHProductService ahProductService;
    private final JumboProductService jumboProductService;
    private final OpenFoodFactsProductService openFoodFactsProductService;

    public ProductDto getProductByBarcode(String barcode) {
        // Stream.parallel still preserves order when collecting
        var results = Stream.of(openFoodFactsProductService, ahProductService, jumboProductService)
                .parallel()
                .map(s -> s.getProductByBarcodeAndIgnoreFailure(barcode))
                .collect(toList());
        if (allOptionalsAreEmpty(results)) {
            throw new ProductNotFoundException(barcode);
        }
        var openFoodFactsResult = results.get(0);
        var supermarketResults = getOptionalsIfPresent(results.subList(1, results.size()));
        if (openFoodFactsResult.isEmpty()) {
            return firstElement(supermarketResults);
        }
        // Use image from supermarket results, since those are better
        var result = openFoodFactsResult.get();
        extractFirstImageUrl(supermarketResults).ifPresent(result::setImageUrl);
        return result;
    }

    private Optional<String> extractFirstImageUrl(List<ProductDto> productDtos) {
        return productDtos.stream()
                .map(ProductDto::getImageUrl)
                .filter(Objects::nonNull)
                .findFirst();
    }

    private static <T> List<T> getOptionalsIfPresent(Collection<Optional<T>> optionals) {
        return optionals.stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
    }

    private static <T> boolean allOptionalsAreEmpty(Collection<Optional<T>> optionals) {
        return optionals.stream().allMatch(Optional::isEmpty);
    }
}
