package org.harrygovert.app.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.harrygovert.app.controller.model.AHReceiptSearchResult;
import org.harrygovert.app.controller.model.ProductDto;
import org.harrygovert.connector.ah.AHConnector;
import org.harrygovert.connector.ah.converter.AHProductDetailsToProductDtoConverter;
import org.harrygovert.connector.ah.converter.AHProductToProductDtoConverter;
import org.harrygovert.connector.ah.model.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.harrygovert.connector.ah.AHConnector.AHSearchSpecification;
import static org.springframework.util.CollectionUtils.firstElement;

@Component
@RequiredArgsConstructor
@Slf4j
public class AHProductService extends ProductService {
    private final AHConnector ahConnector;
    private final AHProductToProductDtoConverter productConverter;
    private final AHProductDetailsToProductDtoConverter productDetailsConverter;

    @Override
    public ProductDto getProductByBarcode(String barcode) {
        return productConverter.convert(ahConnector.getProductByBarcode(barcode), barcode);
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return searchProducts("");
    }

    private List<Product> searchAhProducts(String name) {
        AHSearchSpecification spec = AHSearchSpecification.builder()
                .query(name)
                .build();
        return ahConnector.searchProducts(spec).getProducts();
    }

    @Override
    public List<ProductDto> searchProducts(String name) {
        return searchAhProducts(name).stream()
                .map(productConverter::convert)
                .collect(toList());
    }

    @Override
    public List<ProductDto> searchProductsByCategory(String category) {
        log.warn("At this point it is unsure if searching by category in AH works properly!");
        return searchProducts(category);
    }

    public ProductDto getProductDetails(String webshopId) {
        return productDetailsConverter.convert(ahConnector.getProductDetails(webshopId));
    }

    private Optional<Product> getBestGuessForReceiptSearch(List<Product> candidates, double price) {
        for (Product candidate : candidates) {
            if (candidate.getPriceBeforeBonus() == price) {
                return of(candidate);
            }
        }
        return ofNullable(firstElement(candidates));
    }

    public AHReceiptSearchResult searchReceiptCandidates(String name, double price) {
        List<Product> results = searchAhProducts(name);
        AHReceiptSearchResult result = AHReceiptSearchResult.builder()
                .allCandidates(productConverter.convert(results))
                .build();
        Optional<Product> bestCandidate = getBestGuessForReceiptSearch(results, price);
        log.debug("Best candidate is: {}\nFrom list: {}", bestCandidate, results);
        bestCandidate
                .map(productConverter::convert)
                .ifPresent(result::setBestGuess);
        return result;
    }
}
