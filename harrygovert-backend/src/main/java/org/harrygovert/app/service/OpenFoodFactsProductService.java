package org.harrygovert.app.service;

import lombok.RequiredArgsConstructor;
import org.harrygovert.app.controller.model.EcoscoreDto;
import org.harrygovert.app.controller.model.ProductDto;
import org.harrygovert.app.controller.model.ProductPhotoTypeDto;
import org.harrygovert.connector.openfoodfacts.OpenFoodFactsConnector;
import org.harrygovert.connector.openfoodfacts.converter.OpenFoodFactsProductToEcoscoreDtoConverter;
import org.harrygovert.connector.openfoodfacts.converter.OpenFoodFactsProductToProductDtoConverter;
import org.harrygovert.connector.openfoodfacts.model.Product;
import org.harrygovert.connector.openfoodfacts.model.ProductPhotoType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.lang.Math.pow;
import static java.lang.Math.round;
import static java.lang.Math.sqrt;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.harrygovert.app.controller.model.EcoscoreDto.ecoscoreToGrade;
import static org.harrygovert.connector.openfoodfacts.OpenFoodFactsConnector.OpenFoodFactsSearchSpecification;

@Service
@RequiredArgsConstructor
public class OpenFoodFactsProductService extends ProductService {
    private final OpenFoodFactsConnector openFoodFactsConnector;
    private final OpenFoodFactsProductToEcoscoreDtoConverter productToEcoscoreConverter;
    private final OpenFoodFactsProductToProductDtoConverter productToProductResponseConverter;
    @Value("${app.ecoscore.openfoodfacts.category-average.threshold}")
    private Integer nItemsThreshold;
    @Value("${app.ecoscore.openfoodfacts.use-test-server:false}")
    private boolean useOpenFoodFactsTestServer;

    private static final Map<ProductPhotoTypeDto, ProductPhotoType> PHOTO_TYPE_MAPPING = Map.of(
            ProductPhotoTypeDto.front, ProductPhotoType.front,
            ProductPhotoTypeDto.ingredients, ProductPhotoType.ingredients,
            ProductPhotoTypeDto.nutrition, ProductPhotoType.nutrition,
            ProductPhotoTypeDto.packaging, ProductPhotoType.packaging
    );

    @Override
    public ProductDto getProductByBarcode(String barcode) {
        Product product = openFoodFactsConnector.getProductByBarcode(barcode).getProduct();
        return productToProductResponseConverter.convert(product);
    }

    @Override
    public List<ProductDto> getAllProducts() {
        var spec = OpenFoodFactsSearchSpecification.builder().terms("").build();
        return productToProductResponseConverter.convert(openFoodFactsConnector.searchAllProducts(spec));
    }

    @Override
    public List<ProductDto> searchProducts(String name) {
        var spec = OpenFoodFactsSearchSpecification.builder().terms(name).build();
        return productToProductResponseConverter.convert(openFoodFactsConnector.searchProducts(spec).getProducts());
    }

    @Override
    public List<ProductDto> searchProductsByCategory(String category) {
        var spec = OpenFoodFactsSearchSpecification.builder().terms(category).build();
        return productToProductResponseConverter.convert(openFoodFactsConnector.searchProductsByCategory(spec).getProducts());
    }

    public EcoscoreDto getEcoscoreDataForBarcode(String barcode) {
        Product product = openFoodFactsConnector.getProductByBarcode(barcode).getProduct();
        return getEcoscoreDataForProduct(product).orElse(null);
    }

    private Optional<EcoscoreDto> getEcoscoreDataForProduct(Product product) {
        if (product.hasEcoscoreOrGrade()) {
            return ofNullable(productToEcoscoreConverter.convert(product));
        }
        return ofNullable(product.getCategoriesHierarchy())
                .flatMap(this::getEcoscoreDataForFirstCategoryAboveThreshold);
    }

    private Optional<EcoscoreDto> getEcoscoreDataForFirstCategoryAboveThreshold(List<String> categoryHierarchy) {
        return categoryHierarchy.stream()
                .map(this::getAverageEcoscoreForCategoryIfAboveThreshold)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst();
    }

    private Optional<EcoscoreDto> getAverageEcoscoreForCategoryIfAboveThreshold(String category) {
        List<Integer> ecoscores = getAllEcoscoresForCategory(category);
        if (ecoscores.size() < nItemsThreshold) {
            return empty();
        }
        return getAverageEcoscoreForCategory(category, ecoscores);
    }

    private List<Integer> getAllEcoscoresForCategory(String category) {
        var spec = OpenFoodFactsSearchSpecification.builder().terms(category).build();
        return openFoodFactsConnector.searchProductsByCategory(spec).getProducts().stream()
                .map(Product::getEcoscoreScore)
                .filter(Objects::nonNull)
                .collect(toList());
    }

    private Optional<EcoscoreDto> getAverageEcoscoreForCategory(String category, List<Integer> ecoscores) {
        if (ecoscores.isEmpty()) {
            return empty();
        }
        double averageEcoscore = getAverage(ecoscores);
        double standardDeviation = getStandardDeviation(ecoscores, averageEcoscore);
        int ecoscore = (int) round(averageEcoscore);
        EcoscoreDto result = EcoscoreDto.builder()
                .score(ecoscore)
                .standardDeviation(standardDeviation)
                .grade(ecoscoreToGrade(ecoscore))
                .source("OpenFoodFacts - average of category: " + category)
                .build();
        return of(result);
    }

    public Optional<EcoscoreDto> getAverageEcoscoreForCategory(String category) {
        return getAverageEcoscoreForCategory(category, getAllEcoscoresForCategory(category));
    }

    private static double getAverage(List<Integer> data) {
        return data.stream()
                .mapToInt(s -> s)
                .average()
                .orElseThrow(() -> new IllegalStateException("Unexpected situation - average was empty"));
    }

    private static double getStandardDeviation(List<Integer> data, double average) {
        double variance = data.stream()
                .mapToDouble(s -> pow(s - average, 2))
                .average()
                .orElseThrow(() -> new IllegalStateException("Unexpected situation - variance was empty"));
        return sqrt(variance);
    }

    public void addProductByBarcodeAndName(String barcode, String name, boolean isTest) {
        openFoodFactsConnector.addProductByBarcodeAndName(barcode, name, isTest | useOpenFoodFactsTestServer);
    }

    public void addPhotoToProduct(String barcode, ProductPhotoTypeDto photoType, MultipartFile photo, boolean isTest) {
        if (!PHOTO_TYPE_MAPPING.containsKey(photoType)) {
            throw new IllegalStateException("Unexpected situation - photo type " + photoType + " was not mapped in Service");
        }
        openFoodFactsConnector.addPhotoToProduct(
                barcode,
                PHOTO_TYPE_MAPPING.get(photoType),
                photo,
                isTest | useOpenFoodFactsTestServer
        );
    }
}
