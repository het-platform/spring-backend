package org.harrygovert.connector.jumbo.converter;

import org.harrygovert.app.controller.model.ProductDto;
import org.harrygovert.connector.converter.ProductImageDetailsListToImageUrlConverter;
import org.harrygovert.connector.converter.ToProductDtoConverter;
import org.harrygovert.connector.jumbo.model.Product;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import static java.lang.String.format;

@Mapper(componentModel = "spring", uses = ProductImageDetailsListToImageUrlConverter.class)
public interface JumboProductToProductDtoConverter extends ToProductDtoConverter<Product> {
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "detailsLink", source = "source.id", qualifiedByName = "convertDetailsLink")
    @Mapping(target = "imageUrl", source = "source.imageInfo.primaryView")
    @Mapping(target = "name", source = "source.title")
    ProductDto convert(Product source);

    @Named("convertDetailsLink")
    default String convertDetailsLink(String webshopId) {
        return format("/products/jumbo/%s", webshopId);
    }
}
