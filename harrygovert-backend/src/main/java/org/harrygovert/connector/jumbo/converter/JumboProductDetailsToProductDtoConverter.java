package org.harrygovert.connector.jumbo.converter;

import org.harrygovert.app.config.JumboMappings;
import org.harrygovert.app.controller.model.ProductDto;
import org.harrygovert.connector.converter.ProductImageDetailsListToImageUrlConverter;
import org.harrygovert.connector.converter.ToProductDtoConverter;
import org.harrygovert.connector.jumbo.model.ProductDetails;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.String.format;

@Mapper(componentModel = "spring", uses = {ProductImageDetailsListToImageUrlConverter.class, JumboProductToProductDtoConverter.class})
public abstract class JumboProductDetailsToProductDtoConverter implements ToProductDtoConverter<ProductDetails> {
    @Autowired
    private JumboMappings jumboMapping;

    @Override
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "detailsLink", source = "product.data.id", qualifiedByName = "convertDetailsLink")
    @Mapping(target = "ecoscoreLink", source = "product.data.id", qualifiedByName = "convertEcoscoreByOffCategoryLink")
    @Mapping(target = "imageUrl", source = "product.data.imageInfo.primaryView")
    @Mapping(target = "name", source = "product.data.title")
    public abstract ProductDto convert(ProductDetails source);

    @Named("convertEcoscoreByOffCategoryLink")
    String convertEcoscoreByOffCategoryLink(String webshopId) {
        return jumboMapping.getJumboCategoryForJumboProductId(webshopId)
                .flatMap(jumboMapping::getOffCategoryForJumboCategory)
                .map(offCategory -> format("/products/ecoscore?offCategory=%s", offCategory))
                .orElse(null);
    }
}
