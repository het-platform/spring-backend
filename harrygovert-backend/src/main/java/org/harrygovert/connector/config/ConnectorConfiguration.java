package org.harrygovert.connector.config;

import com.github.mizosoft.methanol.Methanol;
import lombok.SneakyThrows;
import org.harrygovert.connector.ah.AHConnector;
import org.harrygovert.connector.jumbo.JumboConnector;
import org.harrygovert.connector.openfoodfacts.OpenFoodFactsConnector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.InetSocketAddress;
import java.net.ProxySelector;
import java.net.http.HttpClient;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import static java.net.http.HttpClient.Builder;
import static org.harrygovert.connector.util.HttpClientUtil.buildHttpClient;
import static org.harrygovert.connector.util.HttpClientUtil.buildMethanolClient;


@Configuration
public class ConnectorConfiguration {
    public static final int REQUEST_TIMEOUT_S = 5;
    private static final TrustManager ALL_TRUSTING_TRUST_MANAGER = new X509TrustManager() {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    };

    @SneakyThrows
    public static Builder routeViaProxy(Builder clientBuilder) {
        return disableSslVerification(clientBuilder)
                .proxy(ProxySelector.of(new InetSocketAddress("127.0.0.1", 8080)));
    }

    @SneakyThrows
    private static Builder disableSslVerification(Builder clientBuilder) {
        var context = SSLContext.getInstance("TLS");
        context.init(null, new TrustManager[]{ALL_TRUSTING_TRUST_MANAGER}, new SecureRandom());
        return clientBuilder.sslContext(context);
    }

    @Bean
    public HttpClient httpClient() {
        return buildHttpClient(REQUEST_TIMEOUT_S);
    }

    @Bean
    public Methanol methanolHttpClient(HttpClient httpClient) {
        return buildMethanolClient(httpClient);
    }

    @Bean
    public OpenFoodFactsConnector openFoodFactsConnector(
            Methanol methanolHttpClient,
            @Value("${app.ecoscore.openfoodfacts.use-test-server:false}") boolean useOpenFoodFactsTestServer) {
        return new OpenFoodFactsConnector(methanolHttpClient, useOpenFoodFactsTestServer);
    }

    @Bean
    public JumboConnector jumboConnector(Methanol methanolHttpClient) {
        return new JumboConnector(methanolHttpClient);
    }

    @Bean
    public AHConnector ahConnector(Methanol methanolHttpClient) {
        return new AHConnector(methanolHttpClient);
    }
}
