package org.harrygovert.connector.converter;

import org.harrygovert.app.controller.model.ProductDto;
import org.springframework.core.convert.converter.Converter;

import java.util.List;

public interface ToProductDtoConverter<S> extends Converter<S, ProductDto> {
    List<ProductDto> convert(List<S> source);

    default ProductDto convert(S source, String barcode) {
        ProductDto result = convert(source);
        if (result != null) {
            result.setBarcode(barcode);
        }
        return result;
    }
}
