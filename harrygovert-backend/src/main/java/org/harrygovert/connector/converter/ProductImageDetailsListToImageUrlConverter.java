package org.harrygovert.connector.converter;

import org.harrygovert.connector.model.ProductImageDetails;
import org.mapstruct.Mapper;
import org.springframework.core.convert.converter.Converter;

import java.util.List;
import java.util.Objects;

import static java.util.Comparator.comparingInt;

@Mapper(componentModel = "spring")
public interface ProductImageDetailsListToImageUrlConverter extends Converter<List<ProductImageDetails>, String> {
    @Override
    default String convert(List<ProductImageDetails> source) {
        if (source == null) {
            return null;
        }
        return source.stream()
                .filter(Objects::nonNull)
                .max(comparingInt(p -> p.getWidth() * p.getHeight()))
                .map(ProductImageDetails::getUrl)
                .orElse(null);
    }
}
