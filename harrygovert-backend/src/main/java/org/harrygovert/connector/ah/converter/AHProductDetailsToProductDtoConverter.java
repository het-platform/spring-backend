package org.harrygovert.connector.ah.converter;

import org.harrygovert.app.controller.model.ProductDto;
import org.harrygovert.connector.ah.model.ProductDetails;
import org.harrygovert.connector.converter.ProductImageDetailsListToImageUrlConverter;
import org.harrygovert.connector.converter.ToProductDtoConverter;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {ProductImageDetailsListToImageUrlConverter.class, AHProductToProductDtoConverter.class})
public interface AHProductDetailsToProductDtoConverter extends ToProductDtoConverter<ProductDetails> {
    @Override
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "barcode", source = "tradeItem.gtin")
    @Mapping(target = "ecoscoreLink", source = "tradeItem.gtin", qualifiedByName = "convertEcoscoreByBarcodeLink")
    @Mapping(target = "detailsLink", source = "productCard.webshopId", qualifiedByName = "convertDetailsLink")
    @Mapping(target = "imageUrl", source = "productCard.images")
    @Mapping(target = "name", source = "productCard.title")
    ProductDto convert(ProductDetails source);
}
