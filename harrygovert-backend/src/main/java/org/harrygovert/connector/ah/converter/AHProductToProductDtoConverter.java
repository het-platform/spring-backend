package org.harrygovert.connector.ah.converter;

import org.harrygovert.app.controller.model.ProductDto;
import org.harrygovert.connector.ah.model.Product;
import org.harrygovert.connector.converter.ProductImageDetailsListToImageUrlConverter;
import org.harrygovert.connector.converter.ToProductDtoConverter;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import static java.lang.String.format;

@Mapper(componentModel = "spring", uses = ProductImageDetailsListToImageUrlConverter.class)
public interface AHProductToProductDtoConverter extends ToProductDtoConverter<Product> {
    @Override
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "detailsLink", source = "source.webshopId", qualifiedByName = "convertDetailsLink")
    @Mapping(target = "imageUrl", source = "source.images")
    @Mapping(target = "name", source = "source.title")
    ProductDto convert(Product source);

    @Named("convertDetailsLink")
    default String convertDetailsLink(String webshopId) {
        return format("/products/ah/%s", webshopId);
    }

    @Named("convertEcoscoreByBarcodeLink")
    default String convertEcoscoreByBarcodeLink(String barcode) {
        return format("/products/ecoscore?barcode=%s", barcode);
    }
}
