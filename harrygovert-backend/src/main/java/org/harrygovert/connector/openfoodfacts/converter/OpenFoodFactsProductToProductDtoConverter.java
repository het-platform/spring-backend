package org.harrygovert.connector.openfoodfacts.converter;

import org.apache.commons.lang3.StringUtils;
import org.harrygovert.app.controller.model.ProductDto;
import org.harrygovert.app.controller.model.ProductPhotoTypeDto;
import org.harrygovert.connector.converter.ToProductDtoConverter;
import org.harrygovert.connector.openfoodfacts.model.Product;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static org.harrygovert.app.controller.model.ProductPhotoTypeDto.front;
import static org.harrygovert.app.controller.model.ProductPhotoTypeDto.ingredients;
import static org.harrygovert.app.controller.model.ProductPhotoTypeDto.nutrition;
import static org.harrygovert.app.controller.model.ProductPhotoTypeDto.packaging;

@Mapper(componentModel = "spring", uses = OpenFoodFactsProductToEcoscoreDtoConverter.class)
public interface OpenFoodFactsProductToProductDtoConverter extends ToProductDtoConverter<Product> {
    @Override
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "barcode", source = "code")
    @Mapping(target = "ecoscoreDto", source = "source")
    @Mapping(target = "ecoscoreLink", source = "source", qualifiedByName = "convertEcoscoreLink")
    @Mapping(target = "imageUrl", source = "imageUrl")
    @Mapping(target = "openFoodFactsImages", source = "source")
    @Mapping(target = "name", source = "source", qualifiedByName = "convertName")
    ProductDto convert(Product source);

    default Map<ProductPhotoTypeDto, String> convertAllImages(Product source) {
        var result = new HashMap<ProductPhotoTypeDto, String>();
        ofNullable(source.getImageFrontUrl()).ifPresent(url -> result.put(front, url));
        ofNullable(source.getImageIngredientsUrl()).ifPresent(url -> result.put(ingredients, url));
        ofNullable(source.getImageNutritionUrl()).ifPresent(url -> result.put(nutrition, url));
        ofNullable(source.getImagePackagingUrl()).ifPresent(url -> result.put(packaging, url));
        return result;
    }

    @Named("convertEcoscoreLink")
    default String convertEcoscoreLink(Product source) {
        if (source.hasEcoscoreOrGrade()) {
            return format("/products/ecoscore?barcode=%s", source.getCode());
        }
        return ofNullable(source.getCategoriesHierarchy())
                .map(Collection::stream)
                .flatMap(Stream::findFirst)
                .map(category -> format("/products/ecoscore?offCategory=%s", category))
                .orElse(null);
    }

    @Named("convertName")
    default String convertName(Product source) {
        List<String> names = new ArrayList<>();
        names.add(source.getProductName());
        names.addAll(getNonImportedNames(source));
        return getFirstNonBlankEntry(names).orElse("<Unknown>");
    }

    private static List<String> getNonImportedNames(Product source) {
        return source.getLocalizedProductNames().entrySet().stream()
                .filter(OpenFoodFactsProductToProductDtoConverter::entryIsNotImported)
                .map(Entry::getValue)
                .collect(toList());
    }

    private static boolean entryIsNotImported(Entry<String, ?> entry) {
        return !entry.getKey().endsWith("imported");
    }

    private static Optional<String> getFirstNonBlankEntry(List<String> names) {
        return names.stream()
                .filter(StringUtils::isNotBlank)
                .findFirst();
    }
}
