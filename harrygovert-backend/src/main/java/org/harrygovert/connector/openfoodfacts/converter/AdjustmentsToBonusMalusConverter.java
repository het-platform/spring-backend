package org.harrygovert.connector.openfoodfacts.converter;

import org.harrygovert.app.controller.model.EcoscoreDto;
import org.harrygovert.connector.openfoodfacts.model.Adjustments;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.core.convert.converter.Converter;

@Mapper(componentModel = "spring")
public interface AdjustmentsToBonusMalusConverter extends Converter<Adjustments, EcoscoreDto.BonusMalus> {
    @Override
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "originsOfIngredients.combined", source = "originsOfIngredients.value")
    @Mapping(target = "originsOfIngredients.environmentalPerformanceIndex", source = "originsOfIngredients.epiValue")
    @Mapping(target = "originsOfIngredients.transportation", source = "originsOfIngredients.transportationValue")
    @Mapping(target = "packaging", source = "packaging.value")
    @Mapping(target = "productionSystem", source = "productionSystem.value")
    @Mapping(target = "threatenedSpecies", source = "threatenedSpecies.value")
    EcoscoreDto.BonusMalus convert(Adjustments source);
}
