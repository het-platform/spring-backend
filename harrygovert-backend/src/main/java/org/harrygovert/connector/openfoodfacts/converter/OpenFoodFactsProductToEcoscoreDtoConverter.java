package org.harrygovert.connector.openfoodfacts.converter;

import org.harrygovert.app.controller.model.EcoscoreDto;
import org.harrygovert.connector.openfoodfacts.model.Agribalyse;
import org.harrygovert.connector.openfoodfacts.model.Product;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.core.convert.converter.Converter;

import java.util.Optional;

@Mapper(componentModel = "spring", uses = AdjustmentsToBonusMalusConverter.class)
public interface OpenFoodFactsProductToEcoscoreDtoConverter extends Converter<Product, EcoscoreDto> {
    @Override
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "bonusMalus", source = "ecoscoreData.adjustments")
    @Mapping(target = "co2EquivalentPerKg", source = "ecoscoreData.agribalyse.co2Total")
    @Mapping(target = "co2ImpactDistribution", source = "ecoscoreData.agribalyse", qualifiedByName = "convertCo2Distribution")
    @Mapping(target = "environmentalFootprintDistribution", source = "ecoscoreData.agribalyse",
            qualifiedByName = "convertEnvironmentalFootprintDistribution")
    @Mapping(target = "grade", source = "ecoscoreGrade")
    @Mapping(target = "score", source = "ecoscoreScore")
    @Mapping(target = "source", constant = "OpenFoodFacts")
    EcoscoreDto convert(Product source);

    private static Double nullSafePercentage(Double component, double total) {
        return Optional.ofNullable(component)
                .map(c -> 100.0 * c / total)
                .orElse(null);
    }


    @Named("convertCo2Distribution")
    default EcoscoreDto.ImpactDistribution convertCo2Distribution(Agribalyse source) {
        Double co2Total = Optional.ofNullable(source)
                .map(Agribalyse::getCo2Total)
                .orElse(null);
        if (co2Total == null) {
            return null;
        }
        return EcoscoreDto.ImpactDistribution.builder()
                .agriculture(nullSafePercentage(source.getCo2Agriculture(), co2Total))
                .consumption(nullSafePercentage(source.getCo2Consumption(), co2Total))
                .distribution(nullSafePercentage(source.getCo2Distribution(), co2Total))
                .packaging(nullSafePercentage(source.getCo2Packaging(), co2Total))
                .processing(nullSafePercentage(source.getCo2Processing(), co2Total))
                .transportation(nullSafePercentage(source.getCo2Transportation(), co2Total))
                .build();
    }

    @Named("convertEnvironmentalFootprintDistribution")
    default EcoscoreDto.ImpactDistribution convertEnvironmentalFootprintDistribution(Agribalyse source) {
        Double efTotal = Optional.ofNullable(source)
                .map(Agribalyse::getEfTotal)
                .orElse(null);
        if (efTotal == null) {
            return null;
        }
        return EcoscoreDto.ImpactDistribution.builder()
                .agriculture(nullSafePercentage(source.getEfAgriculture(), efTotal))
                .consumption(nullSafePercentage(source.getEfConsumption(), efTotal))
                .distribution(nullSafePercentage(source.getEfDistribution(), efTotal))
                .packaging(nullSafePercentage(source.getEfPackaging(), efTotal))
                .processing(nullSafePercentage(source.getEfProcessing(), efTotal))
                .transportation(nullSafePercentage(source.getEfTransportation(), efTotal))
                .build();
    }
}
