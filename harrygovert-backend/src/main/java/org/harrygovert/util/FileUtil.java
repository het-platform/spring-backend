package org.harrygovert.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.FileNotFoundException;
import java.io.InputStream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FileUtil {
    private static final ResourcePatternResolver RESOURCE_PATTERN_RESOLVER = new PathMatchingResourcePatternResolver();

    @SneakyThrows
    public static InputStream getFile(String path) {
        if (!StringUtils.startsWith(path, "classpath")) {
            path = "classpath*:" + path;
        }
        Resource[] resources = RESOURCE_PATTERN_RESOLVER.getResources(path);
        if (resources.length > 0) {
            return resources[0].getInputStream();
        }
        throw new FileNotFoundException(path);
    }
}
