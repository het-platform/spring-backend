package org.harrygovert;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;

import static org.apache.commons.lang3.StringUtils.startsWith;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileUtil {
    public static Resource getResource(String fileName) {
        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        try {
            String file = !startsWith(fileName, "classpath") ? "classpath*:" + fileName : fileName;
            Resource[] resources = patternResolver.getResources(file);
            if (resources.length > 0) {
                return resources[0];
            }
            throw new IllegalStateException("Cannot find file with resource name " + file);
        } catch (IOException e) {
            throw new IllegalStateException("IOException while reading file with resource name " + fileName, e);
        }
    }
}
