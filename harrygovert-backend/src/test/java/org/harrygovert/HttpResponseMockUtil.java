package org.harrygovert;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.net.http.HttpResponse;

import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HttpResponseMockUtil {

    @SuppressWarnings("unchecked")
    static HttpResponse<String> mockHttpResponse(int statusCode, String payload) {
        HttpResponse<String> response = (HttpResponse<String>) mock(HttpResponse.class, CALLS_REAL_METHODS);
        when(response.statusCode()).thenReturn(statusCode);
        when(response.body()).thenReturn(payload);
        return response;
    }
}
