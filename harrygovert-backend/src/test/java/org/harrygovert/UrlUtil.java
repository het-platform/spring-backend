package org.harrygovert;

import lombok.NoArgsConstructor;

import java.net.URLEncoder;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class UrlUtil {
    public static final String AH_ANON_TOKEN_API_URL = "https://api.ah.nl/mobile-auth/v1/auth/token/anonymous";

    public static String makeSearchUrl(String term) {
        return "/products/search?name=" + urlEncode(term);
    }

    public static String makeDeprecatedEcoscoreByBarcodeUrl(String barcode) {
        return "/products/ecoscore/" + barcode;
    }

    public static String makeProductByBarcodeUrl(String barcode) {
        return format("/products?barcode=%s", barcode);
    }

    public static String makeEcoscoreByBarcodeUrl(String barcode) {
        return format("/products/ecoscore?barcode=%s", barcode);
    }

    public static String makeEcoscoreByOffCategoryUrl(String offCategory) {
        return format("/products/ecoscore?offCategory=%s", urlEncode(offCategory));
    }

    public static String makeProductByOffCategoryUrl(String offCategory) {
        return format("/products/ecoscore?offCategory=%s", urlEncode(offCategory));
    }

    public static String makeAhSearchUrl(String term) {
        return "https://api.ah.nl/mobile-services/product/search/v2?sortOn=RELEVANCE&page=0&size=10&query=" + urlEncode(term);
    }

    public static String makeJumboSearchUrl(String term) {
        return "https://mobileapi.jumbo.com/v16/search?q=" + urlEncode(term) + "&offset=0&limit=10";
    }

    public static String makeOffBarcodeUrl(String barcode) {
        return "https://world.openfoodfacts.net/api/v0/product/" + barcode;
    }

    public static String makeOffSearchUrl(String term) {
        return makeOffSearchUrl(term, 1);
    }

    public static String makeOffSearchUrl(String term, int page) {
        return "https://world.openfoodfacts.net/cgi/search.pl?json=true&search_simple=1&search_terms=" + urlEncode(term) + "&page_size=24&page=" + page + "&skip=0";
    }

    public static String makeOffSearchAllUrl(int page) {
        return "https://world.openfoodfacts.net/cgi/search.pl?json=true&search_simple=1&search_terms=&page_size=1000&page=" + page + "&skip=0";
    }

    public static String makeOffCategorySearchUrl(String category) {
        return "https://world.openfoodfacts.net/category/" + urlEncode(category) + ".json?json=true&page_size=24&page=1";
    }

    public static String makeOffProductAddUrl(String barcode) {
        return makeOffProductAddUrl(barcode, null);
    }

    public static String makeOffProductAddUrl(String barcode, String name) {
        String url = "https://world.openfoodfacts.net/cgi/product_jqm2.pl?code=" + barcode;
        if (name != null) {
            url += "&product_name=" + urlEncode(name);
        }
        return url;
    }

    public static String makeOffProductPhotoAddUrl() {
        return "https://world.openfoodfacts.net/cgi/product_image_upload.pl";
    }

    private static String urlEncode(String value) {
        return URLEncoder.encode(value, UTF_8);
    }
}
