package org.harrygovert;

import com.github.mizosoft.methanol.Methanol;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.harrygovert.app.controller.model.EcoscoreDto;
import org.harrygovert.app.controller.model.ProductDto;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.junit.jupiter.MockitoSettings;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.apache.http.HttpStatus.SC_OK;
import static org.harrygovert.HttpResponseMockUtil.mockHttpResponse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.quality.Strictness.WARN;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = "app.ecoscore.openfoodfacts.category-average.threshold=2")
@ActiveProfiles("test")
@MockitoSettings(strictness = WARN)
public abstract class ApiTest {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    protected String apiUrl;

    @MockBean
    private Methanol methanolHttpClient;

    @LocalServerPort
    private int port;

    @SneakyThrows
    @BeforeEach
    void setApiUrl() {
        apiUrl = "http://localhost:" + port;
        // Ensure unmocked requests fail with something more than an NPE
        doThrow(new IllegalArgumentException("HELP!"))
                .when(methanolHttpClient)
                .send(any(HttpRequest.class), any());
    }

    protected <T, U> ResponseEntity<T> exchange(String path, HttpMethod method, U requestBody, HttpHeaders headers, Class<T> clazz) {
        return restTemplate.exchange(apiUrl + path, method, new HttpEntity<>(requestBody, headers), clazz);
    }

    protected <T, U> ResponseEntity<T> exchange(String path, HttpMethod method, U requestBody, Class<T> clazz) {
        return exchange(path, method, requestBody, new HttpHeaders(), clazz);
    }

    protected <T> ResponseEntity<T> get(String path, Class<T> clazz) {
        return exchange(path, GET, null, clazz);
    }

    protected <T, U> ResponseEntity<T> post(String path, U requestBody, Class<T> clazz) {
        return exchange(path, POST, requestBody, clazz);
    }

    @Deprecated
    protected ResponseEntity<ProductDto> getDeprecatedEcoscoreByBarcode(String barcode) {
        return getDeprecatedEcoscoreByBarcode(barcode, ProductDto.class);
    }

    @Deprecated
    protected <T> ResponseEntity<T> getDeprecatedEcoscoreByBarcode(String barcode, Class<T> responseClass) {
        return get(UrlUtil.makeDeprecatedEcoscoreByBarcodeUrl(barcode), responseClass);
    }

    protected ResponseEntity<ProductDto[]> searchByName(String term) {
        return searchByName(term, ProductDto[].class);
    }

    protected <T> ResponseEntity<T> searchByName(String term, Class<T> clazz) {
        return get(UrlUtil.makeSearchUrl(term), clazz);
    }

    protected ResponseEntity<EcoscoreDto> getEcoscoreByBarcode(String barcode) {
        return getEcoscoreByBarcode(barcode, EcoscoreDto.class);
    }

    protected <T> ResponseEntity<T> getEcoscoreByBarcode(String barcode, Class<T> responseClass) {
        return get(UrlUtil.makeEcoscoreByBarcodeUrl(barcode), responseClass);
    }

    protected ResponseEntity<EcoscoreDto> getEcoscoreByOffCategory(String barcode) {
        return getEcoscoreByOffCategory(barcode, EcoscoreDto.class);
    }

    protected <T> ResponseEntity<T> getEcoscoreByOffCategory(String barcode, Class<T> responseClass) {
        return get(UrlUtil.makeEcoscoreByOffCategoryUrl(barcode), responseClass);
    }

    protected ResponseEntity<ProductDto> getProductByBarcode(String barcode) {
        return getProductByBarcode(barcode, ProductDto.class);
    }

    protected <T> ResponseEntity<T> getProductByBarcode(String barcode, Class<T> responseClass) {
        return get(UrlUtil.makeProductByBarcodeUrl(barcode), responseClass);
    }

    protected void mockResponse(String uri, String payload) {
        mockResponse(uri, SC_OK, payload);
    }

    @SneakyThrows
    protected void mockResponse(String uri, int statusCode, String payload) {
        HttpResponse<String> responseNew = mockHttpResponse(statusCode, payload);
        doReturn(responseNew)
                .when(methanolHttpClient)
                .send(argThat(request -> StringUtils.equals(request.uri().toString(), uri)), any());
    }
}
