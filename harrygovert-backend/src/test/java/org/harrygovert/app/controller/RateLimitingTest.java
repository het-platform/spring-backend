package org.harrygovert.app.controller;

import org.harrygovert.ApiTest;
import org.harrygovert.app.controller.model.ErrorResponse;
import org.harrygovert.app.controller.model.ProductDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.TOO_MANY_REQUESTS;

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = {
        "bucket4j.enabled=true",
        "spring.cache.type=",
        "spring.cache.cache-names[0]=rate-limit-buckets-test",
        "bucket4j.filters[0].cache-name=rate-limit-buckets-test",
        "bucket4j.filters[0].rate-limits[0].bandwidths[0].capacity=1",
        "bucket4j.filters[0].rate-limits[0].bandwidths[0].time=1",
        "bucket4j.filters[0].rate-limits[0].bandwidths[0].unit=minutes"
})
@ActiveProfiles("test")
public class RateLimitingTest extends ApiTest {
    @Test
    void shouldPerformRateLimitingBasedOnIp() {
        mockResponse(ProductControllerTestData.OFF_BARCODE1_URL, ProductControllerTestData.OFF_BARCODE1_PAYLOAD_MINIMAL);

        ResponseEntity<ProductDto> response = getDeprecatedEcoscoreByBarcode(ProductControllerTestData.BARCODE1);
        assertEquals(ProductControllerTestData.PRODUCT1_DTO1_MINIMAL, response.getBody());

        ResponseEntity<ErrorResponse> errorResponse = getDeprecatedEcoscoreByBarcode(ProductControllerTestData.BARCODE1, ErrorResponse.class);
        assertEquals(TOO_MANY_REQUESTS, errorResponse.getStatusCode());
    }
}
