package org.harrygovert.app.controller.model;

import org.junit.jupiter.api.Test;

import static org.harrygovert.app.controller.model.EcoscoreDto.ecoscoreToGrade;
import static org.junit.jupiter.api.Assertions.assertEquals;

class EcoscoreDtoTest {

    @Test
    void scoreToGradeTest() {
        assertEquals(ecoscoreToGrade(-1), "f");
        assertEquals(ecoscoreToGrade(0), "f");
        assertEquals(ecoscoreToGrade(16), "f");
        assertEquals(ecoscoreToGrade(17), "e");
        assertEquals(ecoscoreToGrade(33), "e");
        assertEquals(ecoscoreToGrade(34), "d");
        assertEquals(ecoscoreToGrade(49), "d");
        assertEquals(ecoscoreToGrade(50), "c");
        assertEquals(ecoscoreToGrade(66), "c");
        assertEquals(ecoscoreToGrade(67), "b");
        assertEquals(ecoscoreToGrade(83), "b");
        assertEquals(ecoscoreToGrade(84), "a");
        assertEquals(ecoscoreToGrade(100), "a");
        assertEquals(ecoscoreToGrade(101), "a");
    }
}