package org.harrygovert.app.controller;

import org.harrygovert.ApiTest;
import org.harrygovert.FileUtil;
import org.harrygovert.UrlUtil;
import lombok.SneakyThrows;
import org.apache.http.client.utils.URIBuilder;
import org.harrygovert.app.controller.model.ErrorResponse;
import org.harrygovert.app.controller.model.ProductDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;

import static java.util.Objects.requireNonNull;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;
import static org.harrygovert.UrlUtil.makeOffProductAddUrl;
import static org.harrygovert.UrlUtil.makeOffSearchUrl;
import static org.harrygovert.app.controller.ProductController.SUCCESS_RESPONSE;
import static org.harrygovert.app.controller.ProductController.USE_OFF_TEST_SERVER_HEADER;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA;

public class ProductControllerTest extends ApiTest {
    @BeforeEach
    void setup() {
        mockResponse(UrlUtil.AH_ANON_TOKEN_API_URL, "{\"access_token\": \"TOKEN\"}");
    }

    @Test
    void shouldGetFullProductByBarcode() {
        mockResponse(ProductControllerTestData.OFF_BARCODE1_URL, ProductControllerTestData.OFF_BARCODE1_PAYLOAD_FULL);

        ResponseEntity<ProductDto> response = getProductByBarcode(ProductControllerTestData.BARCODE1);

        assertEquals(response.getBody(), ProductControllerTestData.PRODUCT1_DTO1);
    }

    @Test
    void shouldGetMinimalProductByBarcode() {
        mockResponse(ProductControllerTestData.OFF_BARCODE1_URL, ProductControllerTestData.OFF_BARCODE1_PAYLOAD_MINIMAL);

        ResponseEntity<ProductDto> response = getProductByBarcode(ProductControllerTestData.BARCODE1);

        assertEquals(response.getBody(), ProductControllerTestData.PRODUCT1_DTO1_MINIMAL);
    }

    @Test
    void shouldGetNoProductByBarcode() {
        mockResponse(ProductControllerTestData.OFF_BARCODE1_URL, "{\"code\":\"" + ProductControllerTestData.BARCODE1 + "\",\"status\":0,\"status_verbose\":\"product not found\"}");

        ResponseEntity<ErrorResponse> response = getDeprecatedEcoscoreByBarcode(ProductControllerTestData.BARCODE1, ErrorResponse.class);

        assertEquals(response.getStatusCode().value(), SC_NOT_FOUND);
        assertNotEquals(response.getBody(), null);
        assertEquals(requireNonNull(response.getBody()).getMessage(), "Product with barcode " + ProductControllerTestData.BARCODE1 + " not found");
        assertNotEquals(response.getBody().getTimestamp(), null);
    }

    @Test
    void shouldSearchProductsAndReturnOneFromOff() {
        String term = "croque-monsieur";
        mockResponse(UrlUtil.makeOffSearchUrl(term), "{\"products\":[" + ProductControllerTestData.OFF_PRODUCT1_PAYLOAD_MINIMAL + "]}");
        mockResponse(UrlUtil.makeJumboSearchUrl(term), "{\"products\": {\"data\": []}}");
        mockResponse(UrlUtil.makeAhSearchUrl(term), "{\"products\": [], \"page\": {\"totalPages\": 0}}");
        ResponseEntity<ProductDto[]> response = searchByName(term);

        assertNotEquals(response.getBody(), null);
        assertEquals(requireNonNull(response.getBody()).length, 1);
        assertEquals(response.getBody()[0].getBarcode(), ProductControllerTestData.BARCODE1);
    }

    @Test
    void shouldSearchProductsAndReturnOneFromEachSource() {
        String term = "croque-monsieur";
        mockResponse(UrlUtil.makeOffSearchUrl(term), "{\"products\":[" + ProductControllerTestData.OFF_PRODUCT1_PAYLOAD_MINIMAL + "]}");
        mockResponse(UrlUtil.makeAhSearchUrl(term), "{\"products\":[],\"page\":{\"totalPages\":0}}");
        mockResponse(UrlUtil.makeJumboSearchUrl(term), "{\"products\":{\"data\":[]}}");
        ResponseEntity<ProductDto[]> response = searchByName(term);

        assertNotEquals(response.getBody(), null);
        assertEquals(requireNonNull(response.getBody()).length, 1);
        assertEquals(response.getBody()[0].getBarcode(), ProductControllerTestData.BARCODE1);
    }

    @Test
    void shouldSearchProductsAndReturnNone() {
        String term = "nonexistentstuff";
        mockResponse(UrlUtil.makeOffSearchUrl(term), "{\"products\":[]}");
        mockResponse(UrlUtil.makeAhSearchUrl(term), "{\"products\": [], \"page\": {\"totalPages\": 0}}");
        mockResponse(UrlUtil.makeJumboSearchUrl(term), "{\"products\": {\"data\": []}}");
        ResponseEntity<ProductDto[]> response = searchByName(term);

        assertArrayEquals(response.getBody(), new ProductDto[0]);
    }

    @Test
    void shouldAddNewProductByBarcode() {
        mockResponse(UrlUtil.makeOffProductAddUrl(ProductControllerTestData.BARCODE1), "{\"status\": 1, \"status_verbose\": \"fields saved\"}");
        var response = post("/products/" + ProductControllerTestData.BARCODE1, null, String.class);
        assertEquals(response.getStatusCode().value(), SC_OK);
        assertEquals(response.getBody(), SUCCESS_RESPONSE);
    }

    @Test
    @SneakyThrows
    void shouldAddNewProductByBarcodeAndName() {
        String name = "Thoms speciale pindakaas";
        mockResponse(UrlUtil.makeOffProductAddUrl(ProductControllerTestData.BARCODE1, name), "{\"status\": 1, \"status_verbose\": \"fields saved\"}");
        var url = new URIBuilder("/products/" + ProductControllerTestData.BARCODE1).addParameter("name", name).build();
        var response = post(url.toString(), null, String.class);
        assertEquals(response.getBody(), SUCCESS_RESPONSE);
        assertEquals(response.getStatusCode().value(), SC_OK);
    }

    @Test
    @SneakyThrows
    void shouldAddPhotoToProduct() {
        mockResponse(UrlUtil.makeOffProductPhotoAddUrl(), "{\"status\": \"status ok\"}");

        var multiPartBody = new LinkedMultiValueMap<String, Object>();
        multiPartBody.add("photo", FileUtil.getResource("eco_friendly_car.jpeg"));
        multiPartBody.add("photo_type", "front");

        var headers = new HttpHeaders();
        headers.setContentType(MULTIPART_FORM_DATA);
        headers.set(USE_OFF_TEST_SERVER_HEADER, "true");

        var url = new URIBuilder("/products/" + ProductControllerTestData.BARCODE1 + "/photos").build();
        var response = exchange(url.toString(), POST, multiPartBody, headers, String.class);
        assertEquals(response.getBody(), SUCCESS_RESPONSE);
        assertEquals(response.getStatusCode().value(), SC_OK);
    }
}