package org.harrygovert.app.controller.exception.handler;

import org.harrygovert.ApiTest;
import org.harrygovert.app.controller.model.ErrorResponse;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static java.util.Objects.requireNonNull;
import static org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR;
import static org.harrygovert.app.controller.ProductControllerTestData.BARCODE1;
import static org.harrygovert.app.controller.ProductControllerTestData.OFF_BARCODE1_URL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class GlobalExceptionHandlerTest extends ApiTest {
    @Test
    void shouldReturnError500ForJsonExceptions() {
        mockResponse(OFF_BARCODE1_URL, "{");
        ResponseEntity<ErrorResponse> response = getEcoscoreByBarcode(BARCODE1, ErrorResponse.class);
        assertEquals(response.getStatusCode().value(), SC_INTERNAL_SERVER_ERROR);
        assertNotEquals(response.getBody(), null);
        assertEquals(requireNonNull(response.getBody()).getMessage(), "Unexpected end-of-input: expected close marker for Object (start marker at [Source: (String)\"{\"; line: 1, column: 1])\n at [Source: (String)\"{\"; line: 1, column: 2]");
        assertNotEquals(response.getBody().getTimestamp(), null);
    }
}