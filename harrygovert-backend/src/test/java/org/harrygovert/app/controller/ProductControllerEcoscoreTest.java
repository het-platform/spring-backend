package org.harrygovert.app.controller;

import org.harrygovert.ApiTest;
import org.harrygovert.UrlUtil;
import org.harrygovert.app.controller.model.EcoscoreDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static java.util.stream.Collectors.joining;
import static org.apache.http.HttpStatus.SC_OK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ProductControllerEcoscoreTest extends ApiTest {
    @BeforeEach
    void setup() {
        mockResponse(UrlUtil.AH_ANON_TOKEN_API_URL, "{\"access_token\": \"TOKEN\"}");
    }

    public static String makeOffBarcodePayloadWithoutEcoscore(List<String> categories) {
        String categoryHierarchy = categories.stream()
                .map(c -> "\"" + c + "\"")
                .collect(joining(","));
        return "{\"status\":1,\"product\":{\"code\":\"" + ProductControllerTestData.BARCODE1 + "\",\"image_url\":\"" + ProductControllerTestData.OFF_IMG1_URL + "\",\"product_name\":\"" + ProductControllerTestData.PRODUCT1_NAME + "\",\"categories_hierarchy\":[" + categoryHierarchy + "]}}";
    }

    private static void verifyEcoscoreAverageOfOffCategory(EcoscoreDto ecoscoreDto) {
        assertNotNull(ecoscoreDto);
        assertEquals(ecoscoreDto.getGrade(), "c");
        assertEquals(ecoscoreDto.getScore(), 64);
        assertEquals(ecoscoreDto.getStandardDeviation(), 31.8776959644, 0.000001);
        assertEquals(ecoscoreDto.getSource(), "OpenFoodFacts - average of category: " + ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA);
    }

    @Test
    void shouldGetAverageEcoscoreFromFirstApplicableCategory() {
        mockResponse(ProductControllerTestData.OFF_BARCODE1_URL, makeOffBarcodePayloadWithoutEcoscore(List.of(ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA, ProductControllerTestData.CAT_WITHOUT_ENOUGH_ECOSCORE_DATA)));
        mockResponse(UrlUtil.makeOffCategorySearchUrl(ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA), ProductControllerTestData.OFF_SEARCH_RESULT_WITH_4_ECOSCORES);

        ResponseEntity<EcoscoreDto> response = getEcoscoreByBarcode(ProductControllerTestData.BARCODE1);

        assertEquals(response.getStatusCode().value(), SC_OK);
        verifyEcoscoreAverageOfOffCategory(response.getBody());
    }

    @Test
    void shouldGetAverageEcoscoreWithStandardDeviation0FromCategory() {
        mockResponse(ProductControllerTestData.OFF_BARCODE1_URL, makeOffBarcodePayloadWithoutEcoscore(List.of(ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA)));
        mockResponse(UrlUtil.makeOffCategorySearchUrl(ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA), ProductControllerTestData.OFF_SEARCH_RESULT_WITH_ONLY_2_ECOSCORES);

        ResponseEntity<EcoscoreDto> response = getEcoscoreByBarcode(ProductControllerTestData.BARCODE1);

        assertEquals(response.getStatusCode().value(), SC_OK);
        assertEquals(response.getBody(), ProductControllerTestData.ECOSCORE2_DTO);
    }

    @Test
    void shouldSkipCategoryIfItHasTooFewProducts() {
        mockResponse(ProductControllerTestData.OFF_BARCODE1_URL, makeOffBarcodePayloadWithoutEcoscore(List.of(ProductControllerTestData.CAT_WITHOUT_ENOUGH_ECOSCORE_DATA, ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA)));
        mockResponse(UrlUtil.makeOffCategorySearchUrl(ProductControllerTestData.CAT_WITHOUT_ENOUGH_ECOSCORE_DATA), "{\"products\":[{\"ecoscore_score\":12}]}");
        mockResponse(UrlUtil.makeOffCategorySearchUrl(ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA), ProductControllerTestData.OFF_SEARCH_RESULT_WITH_ONLY_2_ECOSCORES);

        ResponseEntity<EcoscoreDto> response = getEcoscoreByBarcode(ProductControllerTestData.BARCODE1);

        assertEquals(response.getStatusCode().value(), SC_OK);
        assertEquals(response.getBody(), ProductControllerTestData.ECOSCORE2_DTO);
    }

    @Test
    void shouldSkipCategoryIfItHasTooLittleEcoscoreData() {
        mockResponse(ProductControllerTestData.OFF_BARCODE1_URL, makeOffBarcodePayloadWithoutEcoscore(List.of(ProductControllerTestData.CAT_WITHOUT_ENOUGH_ECOSCORE_DATA, ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA)));
        mockResponse(UrlUtil.makeOffCategorySearchUrl(ProductControllerTestData.CAT_WITHOUT_ENOUGH_ECOSCORE_DATA), "{\"products\":[{},{},{}]}");
        mockResponse(UrlUtil.makeOffCategorySearchUrl(ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA), ProductControllerTestData.OFF_SEARCH_RESULT_WITH_ONLY_2_ECOSCORES);

        ResponseEntity<EcoscoreDto> response = getEcoscoreByBarcode(ProductControllerTestData.BARCODE1);

        assertEquals(response.getStatusCode().value(), SC_OK);
        assertEquals(response.getBody(), ProductControllerTestData.ECOSCORE2_DTO);
    }

    @Test
    void shouldGetAverageEcoscoreAndStandardDeviationFromAnyOffCategory() {
        mockResponse(ProductControllerTestData.OFF_BARCODE1_URL, makeOffBarcodePayloadWithoutEcoscore(List.of(ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA, ProductControllerTestData.CAT_WITHOUT_ENOUGH_ECOSCORE_DATA)));
        mockResponse(UrlUtil.makeOffCategorySearchUrl(ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA), ProductControllerTestData.OFF_SEARCH_RESULT_WITH_4_ECOSCORES);

        ResponseEntity<EcoscoreDto> response = getEcoscoreByBarcode(ProductControllerTestData.BARCODE1);

        assertEquals(response.getStatusCode().value(), SC_OK);
        verifyEcoscoreAverageOfOffCategory(response.getBody());
    }

    @Test
    void shouldGetEcoscoreByBarcode() {
        mockResponse(ProductControllerTestData.OFF_BARCODE1_URL, ProductControllerTestData.OFF_BARCODE1_PAYLOAD_MINIMAL);

        ResponseEntity<EcoscoreDto> response = getEcoscoreByBarcode(ProductControllerTestData.BARCODE1);

        assertEquals(response.getBody(), ProductControllerTestData.ECOSCORE1_DTO_MINIMAL);
    }

    @Test
    void shouldGetEcoscoreByOffCategory() {
        mockResponse(UrlUtil.makeOffCategorySearchUrl(ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA), ProductControllerTestData.OFF_SEARCH_RESULT_WITH_4_ECOSCORES);

        ResponseEntity<EcoscoreDto> response = getEcoscoreByOffCategory(ProductControllerTestData.CAT_WITH_ENOUGH_ECOSCORE_DATA);

        verifyEcoscoreAverageOfOffCategory(response.getBody());
    }
}