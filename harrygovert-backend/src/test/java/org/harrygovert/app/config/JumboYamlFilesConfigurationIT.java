package org.harrygovert.app.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest()
@ActiveProfiles("test")
public class JumboYamlFilesConfigurationIT {
    @Autowired
    JumboMappings mapping;

    @Test
    void jumboYamlFilesShouldBeLoadedCorrectly() {
        assertEquals(Optional.of("4294961964"), mapping.getJumboCategoryForJumboProductId("100005PAK"));
        assertEquals(Optional.of("en:frozen-fishes"), mapping.getOffCategoryForJumboCategory("4294925044"));
    }
}
