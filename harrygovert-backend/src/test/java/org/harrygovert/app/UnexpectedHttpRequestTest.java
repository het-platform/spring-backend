package org.harrygovert.app;

import com.github.mizosoft.methanol.Methanol;
import org.harrygovert.ApiTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URI;
import java.net.http.HttpRequest;

import static java.net.http.HttpResponse.BodyHandlers;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UnexpectedHttpRequestTest extends ApiTest {
    @Autowired
    private Methanol methanolHttpClient;

    @Test
    void shouldThrowWhenCausingUnmockedRequest() {
        var request = HttpRequest.newBuilder(URI.create("https://www.google.com")).build();
        var ex = assertThrows(IllegalArgumentException.class, () -> methanolHttpClient.send(request, BodyHandlers.ofString()));
        assertEquals("HELP!", ex.getMessage());
    }
}