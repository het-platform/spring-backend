package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class LocalizedModel {
    protected final Map<String, Map<String, String>> LOCALIZED_PROPNAME_MAP = new HashMap<>();
    @Getter
    protected final Map<String, Object> remainingProperties = new HashMap<>();

    @JsonAnySetter
    protected void setAny(String propertyName, Object propertyValue) {
        Optional<Map<String, String>> localizedProperties = LOCALIZED_PROPNAME_MAP.entrySet().stream()
                .filter(e -> propertyName.startsWith(e.getKey()))
                .findFirst()
                .map(Map.Entry::getValue);
        if (localizedProperties.isPresent()) {
            localizedProperties.get().put(propertyName, String.valueOf(propertyValue));
        } else {
            remainingProperties.put(propertyName, propertyValue);
        }
    }
}
