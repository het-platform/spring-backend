package org.harrygovert.connector.openfoodfacts;

import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.MultipartBodyPublisher;
import com.github.mizosoft.methanol.MultipartBodyPublisher.Part;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.ToString;
import org.apache.http.client.utils.URIBuilder;
import org.harrygovert.connector.AbstractConnector;
import org.harrygovert.connector.model.ApiError;
import org.harrygovert.connector.openfoodfacts.exception.ProductAdditionFailedException;
import org.harrygovert.connector.openfoodfacts.exception.ProductNotFoundInOpenFoodFactsException;
import org.harrygovert.connector.openfoodfacts.exception.ProductPhotoAdditionFailedException;
import org.harrygovert.connector.openfoodfacts.model.OpenFoodFactsAddProductPhotoResult;
import org.harrygovert.connector.openfoodfacts.model.OpenFoodFactsAddProductResult;
import org.harrygovert.connector.openfoodfacts.model.OpenFoodFactsBarcodeResult;
import org.harrygovert.connector.openfoodfacts.model.OpenFoodFactsCategoryResult;
import org.harrygovert.connector.openfoodfacts.model.OpenFoodFactsSearchResult;
import org.harrygovert.connector.openfoodfacts.model.Product;
import org.harrygovert.connector.openfoodfacts.model.ProductPhotoType;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.net.http.HttpRequest;
import java.util.List;

import static java.lang.String.format;
import static java.net.http.HttpRequest.BodyPublishers;
import static java.util.Optional.ofNullable;
import static org.apache.http.HttpHeaders.USER_AGENT;
import static org.harrygovert.connector.util.Base64Util.encodeAsBase64;
import static org.harrygovert.connector.util.HttpClientUtil.buildMethanolClient;
import static org.harrygovert.connector.util.HttpRequestUtil.createPhotoFormHeaders;
import static org.harrygovert.connector.util.HttpRequestUtil.get;
import static org.harrygovert.connector.util.HttpRequestUtil.post;
import static org.harrygovert.connector.util.HttpRequestUtil.urlEncode;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

public class OpenFoodFactsConnector extends AbstractConnector {
    private static final String DEV_URL = "https://world.openfoodfacts.net";
    private static final String PRD_URL = "https://world.openfoodfacts.org";
    private static final String BARCODE_FIND_TEMPLATE = "%s/api/v0/product/%s";
    private static final String SEARCH_TEMPLATE = "%s/cgi/search.pl";
    private static final String ALL_CATEGORIES_TEMPLATE = "%s/categories.json";
    private static final String CATEGORY_SEARCH_TEMPLATE = "%s/category/%s.json";
    private static final String PRODUCT_ADD_TEMPLATE = "%s/cgi/product_jqm2.pl";
    private static final String PHOTO_ADD_TEMPLATE = "%s/cgi/product_image_upload.pl";

    private static final int MAX_PAGE_SIZE = 1000;
    private static final int STATUS_SUCCESS = 1;

    private final boolean useOpenFoodFactsTestServer;

    public OpenFoodFactsConnector() {
        this(DEFAULT_REQUEST_TIMEOUT_S);
    }

    public OpenFoodFactsConnector(boolean useOpenFoodFactsTestServer) {
        this(DEFAULT_REQUEST_TIMEOUT_S, useOpenFoodFactsTestServer);
    }

    public OpenFoodFactsConnector(int timeoutInSeconds) {
        this(buildMethanolClient(timeoutInSeconds));
    }

    public OpenFoodFactsConnector(int timeoutInSeconds, boolean useOpenFoodFactsTestServer) {
        this(buildMethanolClient(timeoutInSeconds), useOpenFoodFactsTestServer);
    }

    public OpenFoodFactsConnector(Methanol methanolHttpClient) {
        this(methanolHttpClient, false);
    }

    public OpenFoodFactsConnector(Methanol methanolHttpClient, boolean useOpenFoodFactsTestServer) {
        super(methanolHttpClient);
        this.useOpenFoodFactsTestServer = useOpenFoodFactsTestServer;
    }

    private String getHost() {
        return getHost(false);
    }

    private String getHost(boolean isTest) {
        return isTest | useOpenFoodFactsTestServer ? DEV_URL : PRD_URL;
    }

    private static HttpRequest.Builder createBaseGetRequestBuilder(URI uri) {
        return get(uri).header(USER_AGENT, FAKE_USER_AGENT);
    }

    private static HttpRequest.Builder createBaseGetRequestBuilder(String url) {
        return createBaseGetRequestBuilder(URI.create(url));
    }

    private void addOffDevBasicAuthenticationHeaderIfNeeded(HttpRequest.Builder builder) {
        addOffDevBasicAuthenticationHeaderIfNeeded(builder, false);
    }

    private void addOffDevBasicAuthenticationHeaderIfNeeded(HttpRequest.Builder builder, boolean isTest) {
        if (isTest | useOpenFoodFactsTestServer) {
            builder.header(AUTHORIZATION, "Basic " + encodeAsBase64("off:off"));
        }
    }

    @Override
    protected Class<? extends ApiError> getErrorModelClass() {
        return OpenFoodFactsBarcodeResult.class;
    }

    public OpenFoodFactsBarcodeResult getProductByBarcode(String barcode) {
        String host = getHost();
        String url = format(BARCODE_FIND_TEMPLATE, host, barcode);
        var builder = createBaseGetRequestBuilder(url);
        addOffDevBasicAuthenticationHeaderIfNeeded(builder);
        var response = performJsonRequest(builder, OpenFoodFactsBarcodeResult.class);
        if (response.getStatus() != STATUS_SUCCESS) {
            throw new ProductNotFoundInOpenFoodFactsException(barcode, response.getStatus(), response.getStatusVerbose());
        }
        return response;
    }

    public OpenFoodFactsCategoryResult getAllCategories() {
        String host = getHost();
        var builder = createBaseGetRequestBuilder(format(ALL_CATEGORIES_TEMPLATE, host));
        addOffDevBasicAuthenticationHeaderIfNeeded(builder);
        return performJsonRequest(builder, OpenFoodFactsCategoryResult.class);
    }

    @SneakyThrows
    public OpenFoodFactsSearchResult searchProducts(OpenFoodFactsSearchSpecification spec) {
        String host = getHost();
        URI uri = new URIBuilder(format(SEARCH_TEMPLATE, host))
                .addParameter("json", "true")
                .addParameter("search_simple", "1")
                .addParameter("search_terms", spec.getTerms())
                .addParameter("page_size", spec.getPageSize() + "")
                .addParameter("page", spec.getPage() + "")
                .addParameter("skip", spec.getSkip() + "")
                .build();
        var builder = createBaseGetRequestBuilder(uri);
        addOffDevBasicAuthenticationHeaderIfNeeded(builder);
        return performJsonRequest(builder, OpenFoodFactsSearchResult.class);
    }

    @SneakyThrows
    public OpenFoodFactsSearchResult searchProductsByCategory(OpenFoodFactsSearchSpecification spec) {
        String host = getHost();
        URI uri = new URIBuilder(String.format(CATEGORY_SEARCH_TEMPLATE, host, urlEncode(spec.getTerms())))
                .addParameter("json", "true")
                .addParameter("page_size", spec.getPageSize() + "")
                .addParameter("page", spec.getPage() + "")
                .build();
        var builder = createBaseGetRequestBuilder(uri);
        addOffDevBasicAuthenticationHeaderIfNeeded(builder);
        return performJsonRequest(builder, OpenFoodFactsSearchResult.class);
    }

    public List<Product> searchAllProducts(OpenFoodFactsSearchSpecification spec) {
        spec.setSkip(0);
        // Paging starts at 1
        spec.setPage(1);
        spec.setPageSize(MAX_PAGE_SIZE);
        OpenFoodFactsSearchResult firstResult = searchProducts(spec);
        List<Product> allProducts = firstResult.getProducts();
        for (int page = 2; page <= firstResult.getPageCount(); page++) {
            spec.setPage(page);
            allProducts.addAll(searchProducts(spec).getProducts());
        }
        return allProducts;
    }

    @SneakyThrows
    public void addProductByBarcodeAndName(String barcode, String name, boolean isTest) {
        var host = getHost(isTest);
        var uriBuilder = new URIBuilder(format(PRODUCT_ADD_TEMPLATE, host))
                .addParameter("code", barcode);
        ofNullable(name).ifPresent(n -> uriBuilder.addParameter("product_name", n));
        var builder = createBaseGetRequestBuilder(uriBuilder.build());
        addOffDevBasicAuthenticationHeaderIfNeeded(builder, isTest);
        var result = performJsonRequest(builder, OpenFoodFactsAddProductResult.class);
        if (!"fields saved".equals(result.getStatusVerbose())) {
            throw new ProductAdditionFailedException(result);
        }
    }

    @SneakyThrows
    public void addPhotoToProduct(String barcode, ProductPhotoType photoType, MultipartFile photo, boolean isTest) {
        var headers = createPhotoFormHeaders("imgupload_" + photoType, photo.getOriginalFilename(), photo.getContentType());
        var inputStream = photo.getInputStream();
        var bodyPublisher = MultipartBodyPublisher.newBuilder()
                .part(Part.create(headers, BodyPublishers.ofInputStream(() -> inputStream)))
                .textPart("code", barcode)
                .textPart("imagefield", photoType.toString())
                .build();
        var host = getHost(isTest);
        var builder = post(format(PHOTO_ADD_TEMPLATE, host), bodyPublisher);
        addOffDevBasicAuthenticationHeaderIfNeeded(builder, isTest);
        var result = performJsonRequest(builder, OpenFoodFactsAddProductPhotoResult.class);
        if (!"status ok".equals(result.getStatus())) {
            throw new ProductPhotoAdditionFailedException(result);
        }
    }

    @Builder
    @Data
    @ToString
    public static class OpenFoodFactsSearchSpecification {
        @NonNull
        private final String terms;
        @Builder.Default
        private int page = 1;
        @Builder.Default
        private int pageSize = 24;
        @Builder.Default
        private int skip = 0;
    }
}
