package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class OpenFoodFactsSearchResult extends LocalizedModel {
    private Integer count;
    private Integer page;
    @JsonProperty("page_count")
    private Integer pageCount;
    @JsonProperty("page_size")
    private Integer pageSize;
    private List<Product> products;
    private Integer skip;
}
