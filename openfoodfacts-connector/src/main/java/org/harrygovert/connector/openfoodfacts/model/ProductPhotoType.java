package org.harrygovert.connector.openfoodfacts.model;

public enum ProductPhotoType {
    front, ingredients, nutrition, packaging;

    @Override
    public String toString() {
        return name();
    }
}
