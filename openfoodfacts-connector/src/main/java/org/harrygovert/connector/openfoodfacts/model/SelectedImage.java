package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class SelectedImage extends LocalizedModel {
    private Map<String, String> display;
    private Map<String, String> small;
    private Map<String, String> thumb;
}
