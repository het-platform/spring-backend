package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@EqualsAndHashCode(callSuper = true)
@Data
public class EcoscoreData extends LocalizedModel {
    private Adjustments adjustments;
    private Agribalyse agribalyse;
    @JsonProperty("ecoscore_not_applicable_for_category")
    private String ecoscoreNotApplicableForCategory;
    private String grade;
    private Map<String, String> localizedGrades = new HashMap<>();
    private Map<String, String> localizedScores = new HashMap<>();
    private Missing missing;
    @JsonProperty("missing_agribalyse_match_warning")
    private Integer missingAgribalyseMatchWarning;
    @JsonProperty("missing_data_warning")
    private Integer missingDataWarning;
    @JsonProperty("missing_key_data")
    private Integer missingKeyData;
    private String status;

    {
        LOCALIZED_PROPNAME_MAP.put("grade", localizedGrades);
        LOCALIZED_PROPNAME_MAP.put("score", localizedScores);
    }

    public String getGrade() {
        if (isNotEmpty(grade)) {
            return grade;
        }
        return localizedGrades.keySet().stream()
                .filter(k -> k.contains("nl"))
                .map(localizedGrades::get)
                .findFirst()
                .orElse(
                        localizedGrades.values().stream()
                                .findFirst()
                                .orElse(null));
    }
}
