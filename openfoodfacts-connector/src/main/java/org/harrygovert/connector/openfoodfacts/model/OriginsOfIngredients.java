package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class OriginsOfIngredients extends LocalizedModel {
    @JsonProperty("aggregated_origins")
    private List<AggregatedOrigin> aggregatedOrigins;
    @JsonProperty("epi_score")
    private Double epiScore;
    @JsonProperty("epi_value")
    private Integer epiValue;
    @JsonProperty("origins_from_origins_field")
    private List<String> originsFromOriginsField;
    @JsonProperty("transportation_score")
    private Double transportationScore;
    @JsonProperty("transportation_scores")
    private Map<CountryCode, Double> transportationScores;
    @JsonProperty("transportation_value")
    private Integer transportationValue;
    @JsonProperty("transportation_values")
    private Map<CountryCode, Integer> transportationValues;
    @JsonProperty("value")
    private Integer value;
    @JsonProperty("values")
    private Map<CountryCode, Integer> values;
    private String warning;
}
