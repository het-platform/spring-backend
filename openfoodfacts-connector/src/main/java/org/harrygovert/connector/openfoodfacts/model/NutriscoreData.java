package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigInteger;

@EqualsAndHashCode(callSuper = true)
@Data
public class NutriscoreData extends LocalizedModel {
    private BigInteger energy;
    @JsonProperty("energy_points")
    private Integer energyPoints;
    @JsonProperty("energy_value")
    private BigInteger energyValue;
    private Double fiber;
    @JsonProperty("fiber_points")
    private Integer fiberPoints;
    @JsonProperty("fiber_value")
    private Double fiberValue;
    @JsonProperty("fruits_vegetables_nuts_colza_walnut_olive_oils")
    private Double fruitsVegetablesNutsColzaWalnutOliveOils;
    @JsonProperty("fruits_vegetables_nuts_colza_walnut_olive_oils_points")
    private Integer fruitsVegetablesNutsColzaWalnutOliveOilsPoints;
    @JsonProperty("fruits_vegetables_nuts_colza_walnut_olive_oils_value")
    private Double fruitsVegetablesNutsColzaWalnutOliveOilsValue;
    private String grade;
    @JsonProperty("is_beverage")
    private Integer isBeverage;
    @JsonProperty("is_cheese")
    private Integer isCheese;
    @JsonProperty("is_fat")
    private Integer isFat;
    @JsonProperty("is_water")
    private Integer isWater;
    @JsonProperty("negative_points")
    private Integer negativePoints;
    @JsonProperty("positive_points")
    private Integer positivePoints;
    private Double proteins;
    @JsonProperty("proteins_points")
    private Integer proteinsPoints;
    @JsonProperty("proteins_value")
    private Double proteinsValue;
    @JsonProperty("saturated_fat")
    private Double saturatedFat;
    @JsonProperty("saturated_fat_points")
    private Integer saturatedFatPoints;
    @JsonProperty("saturated_fat_ratio")
    private Double saturatedFatRatio;
    @JsonProperty("saturated_fat_ratio_points")
    private Integer saturatedFatRatioPoints;
    @JsonProperty("saturated_fat_ratio_value")
    private Double saturatedFatRatioValue;
    @JsonProperty("saturated_fat_value")
    private Double saturatedFatValue;
    private Integer score;
    private Double sodium;
    @JsonProperty("sodium_points")
    private Integer sodiumPoints;
    @JsonProperty("sodium_value")
    private Double sodiumValue;
    private Double sugars;
    @JsonProperty("sugars_points")
    private Integer sugarsPoints;
    @JsonProperty("sugars_value")
    private Double sugarsValue;
}
