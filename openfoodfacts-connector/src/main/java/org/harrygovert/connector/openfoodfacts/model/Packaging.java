package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class Packaging extends LocalizedModel {
    @JsonProperty("ecoscore_material_score")
    private Double ecoscoreMaterialScore;
    @JsonProperty("ecoscore_shape_ratio")
    private Double ecoscoreShapeRatio;
    private String material;
    @JsonProperty("non_recyclable_and_non_biodegradable")
    private String nonRecyclableAndNonBiodegradable;
    @JsonProperty("non_recyclable_and_non_biodegradable_materials")
    private Integer nonRecyclableAndNonBiodegradableMaterials;
    private Double number;
    private List<Packaging> packagings;
    private Double score;
    private String shape;
    private Double value;
    private String warning;
}
