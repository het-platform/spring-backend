package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class NutrientLevels extends LocalizedModel {
    private String fat;
    private String salt;
    @JsonProperty("saturated-fat")
    private String saturatedFat;
    private String sugars;
}
