package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static lombok.AccessLevel.NONE;
import static lombok.AccessLevel.PUBLIC;
import static org.apache.commons.lang3.ObjectUtils.anyNotNull;
import static org.apache.commons.lang3.StringUtils.equalsAny;

@EqualsAndHashCode(callSuper = true)
@Data
// Lombok Builder leads to "too many parameters" error...
@Accessors(chain = true)
@AllArgsConstructor(access = NONE)
@NoArgsConstructor(access = PUBLIC)
public class Product extends LocalizedModel {
    private final Map<String, String> localizedAbbreviatedProductNames = new HashMap<>();
    private final Map<String, String> localizedConservationConditions = new HashMap<>();
    private final Map<String, String> localizedCustomerServices = new HashMap<>();
    private final Map<String, String> localizedGenericNames = new HashMap<>();
    private final Map<String, String> localizedIngredientsTexts = new HashMap<>();
    private final Map<String, String> localizedIngredientsTextsWithAllergens = new HashMap<>();
    private final Map<String, String> localizedNutritionGrades = new HashMap<>();
    private final Map<String, String> localizedOrigins = new HashMap<>();
    private final Map<String, String> localizedOtherInformation = new HashMap<>();
    private final Map<String, String> localizedPackagingTexts = new HashMap<>();
    private final Map<String, String> localizedPreparations = new HashMap<>();
    private final Map<String, String> localizedProducers = new HashMap<>();
    private final Map<String, String> localizedProductNames = new HashMap<>();
    private final Map<String, String> localizedRecipeIdeas = new HashMap<>();
    private final Map<String, String> localizedRecyclingInstructionsToDiscard = new HashMap<>();
    private final Map<String, String> localizedRecyclingInstructionsToRecycle = new HashMap<>();
    @JsonProperty("_id")
    private String _id;
    @JsonProperty("abbreviated_product_name")
    private String abbreviatedProductName;
    @JsonProperty("added_countries_tags")
    private List<String> addedCountriesTags;
    @JsonProperty("additives_debug_tags")
    private List<String> additivesDebugTags;
    @JsonProperty("additives_n")
    private Integer additivesN;
    @JsonProperty("additives_old_n")
    private Integer additivesOldN;
    @JsonProperty("additives_old_tags")
    private List<String> additivesOldTags;
    @JsonProperty("additives_original_tags")
    private List<String> additivesOriginalTags;
    @JsonProperty("additives_prev_original_tags")
    private List<String> additivesPrevOriginalTags;
    @JsonProperty("additives_tags")
    private List<String> additivesTags;
    @JsonProperty("additives_tags_n")
    private Object additivesTagsN;
    private String allergens;
    @JsonProperty("allergens_debug_tags")
    private List<String> allergensDebugTags;
    @JsonProperty("allergens_from_ingredients")
    private String allergensFromIngredients;
    @JsonProperty("allergens_from_user")
    private String allergensFromUser;
    @JsonProperty("allergens_hierarchy")
    private List<String> allergensHierarchy;
    @JsonProperty("allergens_imported")
    private String allergensImported;
    @JsonProperty("allergens_lc")
    private String allergensLc;
    @JsonProperty("allergens_tags")
    private List<String> allergensTags;
    @JsonProperty("amino_acids_prev_tags")
    private List<String> aminoAcidsPrevTags;
    @JsonProperty("amino_acids_tags")
    private List<String> aminoAcidsTags;
    @JsonProperty("brand_owner")
    private String brandOwner;
    @JsonProperty("brand_owner_imported")
    private String brandOwnerImported;
    private String brands;
    @JsonProperty("brands_debug_tags")
    private List<String> brandsDebugTags;
    @JsonProperty("brands_imported")
    private String brandsImported;
    @JsonProperty("brands_tags")
    private List<String> brandsTags;
    @JsonProperty("carbon_footprint_from_known_ingredients_debug")
    private String carbonFootprintFromKnownIngredientsDebug;
    @JsonProperty("carbon_footprint_percent_of_known_ingredients")
    private Double carbonFootprintPercentOfKnownIngredients;
    private String categories;
    @JsonProperty("categories_hierarchy")
    private List<String> categoriesHierarchy;
    @JsonProperty("categories_imported")
    private String categoriesImported;
    @JsonProperty("categories_lc")
    private String categoriesLc;
    @JsonProperty("categories_old")
    private String categoriesOld;
    @JsonProperty("categories_properties")
    private CategoriesProperties categoriesProperties;
    @JsonProperty("categories_properties_tags")
    private List<String> categoriesPropertiesTags;
    @JsonProperty("categories_tags")
    private List<String> categoriesTags;
    @JsonProperty("category_properties")
    private CategoryProperties categoryProperties;
    private String checked;
    private List<String> checkers;
    @JsonProperty("checkers_tags")
    private List<String> checkersTags;
    @JsonProperty("ciqual_food_name_tags")
    private List<String> ciqualFoodNameTags;
    @JsonProperty("cities_tags")
    private List<String> citiesTags;
    private String code;
    @JsonProperty("codes_tags")
    private List<String> codesTags;
    @JsonProperty("compared_to_category")
    private String comparedToCategory;
    private Integer complete;
    @JsonProperty("completed_t")
    private Integer completedT;
    private Double completeness;
    @JsonProperty("conservation_conditions")
    private String conservationConditions;
    private List<String> correctors;
    @JsonProperty("correctors_tags")
    private List<String> correctorsTags;
    private String countries;
    @JsonProperty("countries_beforescanbot")
    private String countriesBeforescanbot;
    @JsonProperty("countries_debug_tags")
    private List<String> countriesDebugTags;
    @JsonProperty("countries_hierarchy")
    private List<String> countriesHierarchy;
    @JsonProperty("countries_imported")
    private String countriesImported;
    @JsonProperty("countries_lc")
    private String countriesLc;
    @JsonProperty("countries_tags")
    private List<String> countriesTags;
    @JsonProperty("created_t")
    private Integer createdT;
    private String creator;
    @JsonProperty("customer_service")
    private String customerService;
    @JsonProperty("customer_service_fr_imported")
    private String customerServiceFrImported;
    @JsonProperty("data_quality_bugs_tags")
    private List<String> dataQualityBugsTags;
    @JsonProperty("data_quality_errors_tags")
    private List<String> dataQualityErrorsTags;
    @JsonProperty("data_quality_info_tags")
    private List<String> dataQualityInfoTags;
    @JsonProperty("data_quality_tags")
    private List<String> dataQualityTags;
    @JsonProperty("data_quality_warnings_tags")
    private List<String> dataQualityWarningsTags;
    @JsonProperty("data_sources")
    private String dataSources;
    @JsonProperty("data_sources_imported")
    private String dataSourcesImported;
    @JsonProperty("data_sources_tags")
    private List<String> dataSourcesTags;
    @JsonProperty("debug_param_sorted_langs")
    private List<String> debugParamSortedLangs;
    @JsonProperty("debug_tags")
    private List<String> debugTags;
    private String downgraded;
    @JsonProperty("ecoscore_data")
    private EcoscoreData ecoscoreData;
    @JsonProperty("ecoscore_grade")
    private String ecoscoreGrade;
    @JsonProperty("ecoscore_score")
    private Integer ecoscoreScore;
    @JsonProperty("ecoscore_tags")
    private List<String> ecoscoreTags;
    private List<String> editors;
    @JsonProperty("editors_tags")
    private List<String> editorsTags;
    @JsonProperty("emb_code")
    private String embCode;
    @JsonProperty("emb_codes")
    private String embCodes;
    @JsonProperty("emb_codes_20141016")
    private String embCodes20141016;
    @JsonProperty("emb_codes_debug_tags")
    private List<String> embCodesDebugTags;
    @JsonProperty("emb_codes_imported")
    private String embCodesImported;
    @JsonProperty("emb_codes_orig")
    private String embCodesOrig;
    @JsonProperty("emb_codes_tags")
    private List<String> embCodesTags;
    @JsonProperty("entry_dates_tags")
    private List<String> entryDatesTags;
    @JsonProperty("environment_impact_level")
    private String environmentImpactLevel;
    @JsonProperty("environment_impact_level_tags")
    private List<String> environmentImpactLevelTags;
    @JsonProperty("expiration_date")
    private String expirationDate;
    @JsonProperty("expiration_date_debug_tags")
    private List<String> expirationDateDebugTags;
    @JsonProperty("food_groups")
    private String foodGroups;
    @JsonProperty("food_groups_tags")
    private List<String> foodGroupsTags;
    @JsonProperty("forest_footprint_data")
    private FootprintData forestFootprintData;
    @JsonProperty("fruits-vegetables-nuts_100g_estimate")
    private Double fruitsVegetablesNuts100gEstimate;
    @JsonProperty("generic_name")
    private String genericName;
    @JsonProperty("generic_name_en")
    private String genericNameEn;
    @JsonProperty("generic_name_en_debug_tags")
    private List<String> genericNameEnDebugTags;
    private String id;
    @JsonProperty("image_front_small_url")
    private String imageFrontSmallUrl;
    @JsonProperty("image_front_thumb_url")
    private String imageFrontThumbUrl;
    @JsonProperty("image_front_url")
    private String imageFrontUrl;
    @JsonProperty("image_ingredients_small_url")
    private String imageIngredientsSmallUrl;
    @JsonProperty("image_ingredients_thumb_url")
    private String imageIngredientsThumbUrl;
    @JsonProperty("image_ingredients_url")
    private String imageIngredientsUrl;
    @JsonProperty("image_nutrition_small_url")
    private String imageNutritionSmallUrl;
    @JsonProperty("image_nutrition_thumb_url")
    private String imageNutritionThumbUrl;
    @JsonProperty("image_nutrition_url")
    private String imageNutritionUrl;
    @JsonProperty("image_packaging_small_url")
    private String imagePackagingSmallUrl;
    @JsonProperty("image_packaging_thumb_url")
    private String imagePackagingThumbUrl;
    @JsonProperty("image_packaging_url")
    private String imagePackagingUrl;
    @JsonProperty("image_small_url")
    private String imageSmallUrl;
    @JsonProperty("image_thumb_url")
    private String imageThumbUrl;
    @JsonProperty("image_url")
    private String imageUrl;
    private Map<String, Image> images;
    private List<String> informers;
    @JsonProperty("informers_tags")
    private List<String> informersTags;
    private List<Ingredient> ingredients;
    @JsonProperty("ingredients_analysis_tags")
    private List<String> ingredientsAnalysisTags;
    @JsonProperty("ingredients_debug")
    private List<String> ingredientsDebug;
    @JsonProperty("ingredients_from_or_that_may_be_from_palm_oil_n")
    private Integer ingredientsFromOrThatMayBeFromPalmOilN;
    @JsonProperty("ingredients_from_palm_oil_n")
    private Integer ingredientsFromPalmOilN;
    @JsonProperty("ingredients_from_palm_oil_tags")
    private List<String> ingredientsFromPalmOilTags;
    @JsonProperty("ingredients_hierarchy")
    private List<String> ingredientsHierarchy;
    @JsonProperty("ingredients_ids_debug")
    private List<String> ingredientsIdsDebug;
    @JsonProperty("ingredients_n")
    private Integer ingredientsN;
    @JsonProperty("ingredients_n_tags")
    private List<String> ingredientsNTags;
    @JsonProperty("ingredients_original_tags")
    private List<String> ingredientsOriginalTags;
    @JsonProperty("ingredients_percent_analysis")
    private Integer ingredientsPercentAnalysis;
    @JsonProperty("ingredients_tags")
    private List<String> ingredientsTags;
    @JsonProperty("ingredients_text")
    private String ingredientsText;
    @JsonProperty("ingredients_text_debug")
    private String ingredientsTextDebug;
    @JsonProperty("ingredients_text_en_debug_tags")
    private List<String> ingredientsTextEnDebugTags;
    @JsonProperty("ingredients_text_en_imported")
    private String ingredientsTextEnImported;
    @JsonProperty("ingredients_text_with_allergens")
    private String ingredientsTextWithAllergens;
    @JsonProperty("ingredients_that_may_be_from_palm_oil_n")
    private Integer ingredientsThatMayBeFromPalmOilN;
    @JsonProperty("ingredients_that_may_be_from_palm_oil_tags")
    private List<String> ingredientsThatMayBeFromPalmOilTags;
    @JsonProperty("interface_version_created")
    private String interfaceVersionCreated;
    @JsonProperty("interface_version_modified")
    private String interfaceVersionModified;
    @JsonProperty("_keywords")
    private List<String> keywords;
    @JsonProperty("known_ingredients_n")
    private Integer knownIngredientsN;
    private String labels;
    @JsonProperty("labels_debug_tags")
    private List<String> labelsDebugTags;
    @JsonProperty("labels_hierarchy")
    private List<String> labelsHierarchy;
    @JsonProperty("labels_imported")
    private String labelsImported;
    @JsonProperty("labels_lc")
    private String labelsLc;
    @JsonProperty("labels_old")
    private String labelsOld;
    @JsonProperty("labels_prev_hierarchy")
    private List<String> labelsPrevHierarchy;
    @JsonProperty("labels_prev_tags")
    private List<String> labelsPrevTags;
    @JsonProperty("labels_tags")
    private List<String> labelsTags;
    private String lang;
    @JsonProperty("lang_debug_tags")
    private List<String> langDebugTags;
    @JsonProperty("lang_imported")
    private String langImported;
    private Map<String, Integer> languages;
    @JsonProperty("languages_codes")
    private Map<String, Integer> languagesCodes;
    @JsonProperty("languages_hierarchy")
    private List<String> languagesHierarchy;
    @JsonProperty("languages_tags")
    private List<String> languagesTags;
    @JsonProperty("last_check_dates_tags")
    private List<String> lastCheckDatesTags;
    @JsonProperty("last_checked_t")
    private Integer lastCheckedT;
    @JsonProperty("last_checker")
    private String lastChecker;
    @JsonProperty("last_edit_dates_tags")
    private List<String> lastEditDatesTags;
    @JsonProperty("last_editor")
    private String lastEditor;
    @JsonProperty("last_image_dates_tags")
    private List<String> lastImageDatesTags;
    @JsonProperty("last_image_t")
    private Integer lastImageT;
    @JsonProperty("last_modified_by")
    private String lastModifiedBy;
    @JsonProperty("last_modified_t")
    private Integer lastModifiedT;
    private String lc;
    @JsonProperty("lc_imported")
    private String lcImported;
    private String link;
    @JsonProperty("link_debug_tags")
    private List<String> linkDebugTags;
    @JsonProperty("main_countries_tags")
    private List<String> mainCountriesTags;
    @JsonProperty("manufacturing_places")
    private String manufacturingPlaces;
    @JsonProperty("manufacturing_places_debug_tags")
    private List<String> manufacturingPlacesDebugTags;
    @JsonProperty("manufacturing_places_tags")
    private List<String> manufacturingPlacesTags;
    @JsonProperty("max_imgid")
    private String maxImgid;
    @JsonProperty("minerals_prev_tags")
    private List<String> mineralsPrevTags;
    @JsonProperty("minerals_tags")
    private List<String> mineralsTags;
    @JsonProperty("misc_tags")
    private List<String> miscTags;
    @JsonProperty("net_weight_unit")
    private String netWeightUnit;
    @JsonProperty("net_weight_value")
    private Double netWeightValue;
    @JsonProperty("new_additives_n")
    private Integer newAdditivesN;
    @JsonProperty("no_nutrition_data")
    private String noNutritionData;
    @JsonProperty("no_nutrition_data_imported")
    private String noNutritionDataImported;
    @JsonProperty("not_names_tags")
    private List<String> notNamesTags;
    @JsonProperty("nova_group")
    private Integer novaGroup;
    @JsonProperty("nova_group_debug")
    private String novaGroupDebug;
    @JsonProperty("nova_group_tags")
    private List<String> novaGroupTags;
    @JsonProperty("nova_groups")
    private String novaGroups;
    @JsonProperty("nova_groups_tags")
    private List<String> novaGroupsTags;
    @JsonProperty("nucleotides_prev_tags")
    private List<String> nucleotidesPrevTags;
    @JsonProperty("nucleotides_tags")
    private List<String> nucleotidesTags;
    @JsonProperty("nutrient_levels")
    private NutrientLevels nutrientLevels;
    @JsonProperty("nutrient_levels_tags")
    private List<String> nutrientLevelsTags;
    private Nutriments nutriments;
    @JsonProperty("nutriscore_data")
    private NutriscoreData nutriscoreData;
    @JsonProperty("nutriscore_grade")
    private String nutriscoreGrade;
    @JsonProperty("nutriscore_grade_producer")
    private String nutriscoreGradeProducer;
    @JsonProperty("nutriscore_grade_producer_imported")
    private String nutriscoreGradeProducerImported;
    @JsonProperty("nutriscore_score")
    private Integer nutriscoreScore;
    @JsonProperty("nutriscore_score_opposite")
    private Integer nutriscoreScoreOpposite;
    @JsonProperty("nutrition_data")
    private String nutritionData;
    @JsonProperty("nutrition_data_per")
    private String nutritionDataPer;
    @JsonProperty("nutrition_data_per_debug_tags")
    private List<String> nutritionDataPerDebugTags;
    @JsonProperty("nutrition_data_per_imported")
    private String nutritionDataPerImported;
    @JsonProperty("nutrition_data_prepared")
    private String nutritionDataPrepared;
    @JsonProperty("nutrition_data_prepared_per")
    private String nutritionDataPreparedPer;
    @JsonProperty("nutrition_data_prepared_per_debug_tags")
    private List<String> nutritionDataPreparedPerDebugTags;
    @JsonProperty("nutrition_data_prepared_per_imported")
    private String nutritionDataPreparedPerImported;
    @JsonProperty("nutrition_facts_100g_fr_imported")
    private String nutritionFacts100GFrImported;
    @JsonProperty("nutrition_facts_serving_fr_imported")
    private String nutritionFactsServingFrImported;
    @JsonProperty("nutrition_grades")
    private String nutritionGrades;
    @JsonProperty("nutrition_grades_tags")
    private List<String> nutritionGradesTags;
    @JsonProperty("nutrition_score_beverage")
    private Integer nutritionScoreBeverage;
    @JsonProperty("nutrition_score_debug")
    private String nutritionScoreDebug;
    @JsonProperty("nutrition_score_warning_fruits_vegetables_nuts_estimate")
    private Integer nutritionScoreWarningFruitsVegetablesNutsEstimate;
    @JsonProperty("nutrition_score_warning_fruits_vegetables_nuts_estimate_from_ingredients")
    private Double nutritionScoreWarningFruitsVegetablesNutsEstimateFromIngredients;
    @JsonProperty("nutrition_score_warning_fruits_vegetables_nuts_estimate_from_ingredients_value")
    private Double nutritionScoreWarningFruitsVegetablesNutsEstimateFromIngredientsValue;
    @JsonProperty("nutrition_score_warning_fruits_vegetables_nuts_from_category")
    private Double nutritionScoreWarningFruitsVegetablesNutsFromCategory;
    @JsonProperty("nutrition_score_warning_fruits_vegetables_nuts_from_category_value")
    private Double nutritionScoreWarningFruitsVegetablesNutsFromCategoryValue;
    @JsonProperty("nutrition_score_warning_no_fiber")
    private Integer nutritionScoreWarningNoFiber;
    @JsonProperty("nutrition_score_warning_no_fruits_vegetables_nuts")
    private Integer nutritionScoreWarningNoFruitsVegetablesNuts;
    private String obsolete;
    @JsonProperty("obsolete_imported")
    private String obsoleteImported;
    @JsonProperty("obsolete_since_date")
    private String obsoleteSinceDate;
    private String origin;
    @JsonProperty("origin_fr_imported")
    private String originFrImported;
    private String origins;
    @JsonProperty("origins_hierarchy")
    private List<String> originsHierarchy;
    @JsonProperty("origins_lc")
    private String originsLc;
    @JsonProperty("origins_old")
    private String originsOld;
    @JsonProperty("origins_tags")
    private List<String> originsTags;
    @JsonProperty("other_nutritional_substances_prev_tags")
    private List<String> otherNutritionalSubstancesPrevTags;
    @JsonProperty("other_nutritional_substances_tags")
    private List<String> otherNutritionalSubstancesTags;
    private String owner;
    @JsonProperty("owner_fields")
    private Map<String, String> ownerFields;
    @JsonProperty("owners_tags")
    private String ownersTags;
    private String packaging;
    @JsonProperty("packaging_debug_tags")
    private List<String> packagingDebugTags;
    @JsonProperty("packaging_hierarchy")
    private List<String> packagingHierarchy;
    @JsonProperty("packaging_imported")
    private String packagingImported;
    @JsonProperty("packaging_lc")
    private String packagingLc;
    @JsonProperty("packaging_old")
    private String packagingOld;
    @JsonProperty("packaging_tags")
    private List<String> packagingTags;
    @JsonProperty("packaging_text_id")
    private String packagingTextId;
    @JsonProperty("packagings")
    private List<Packaging> packagings;
    @JsonProperty("periods_after_opening")
    private String periodsAfterOpening;
    @JsonProperty("periods_after_opening_hierarchy")
    private List<String> periodsAfterOpeningHierarchy;
    @JsonProperty("periods_after_opening_imported")
    private String periodsAfterOpeningImported;
    @JsonProperty("periods_after_opening_lc")
    private String periodsAfterOpeningLc;
    @JsonProperty("periods_after_opening_tags")
    private List<String> periodsAfterOpeningTags;
    @JsonProperty("photographers")
    private List<String> photographers;
    @JsonProperty("photographers_tags")
    private List<String> photographersTags;
    @JsonProperty("pnns_groups_1")
    private String pnnsGroups1;
    @JsonProperty("pnns_groups_1_tags")
    private List<String> pnnsGroups1Tags;
    @JsonProperty("pnns_groups_2")
    private String pnnsGroups2;
    @JsonProperty("pnns_groups_2_tags")
    private List<String> pnnsGroups2Tags;
    @JsonProperty("popularity_key")
    private Long popularityKey;
    @JsonProperty("popularity_tags")
    private List<String> popularityTags;
    private String preparation;
    private String producer;
    @JsonProperty("producer_product_id")
    private String producerProductId;
    @JsonProperty("producer_product_id_imported")
    private String producerProductIdImported;
    @JsonProperty("producer_version_id")
    private String producerVersionId;
    @JsonProperty("producer_version_id_imported")
    private String producerVersionIdImported;
    @JsonProperty("product_name")
    private String productName;
    @JsonProperty("product_name_en_debug_tags")
    private List<String> productNameEnDebugTags;
    @JsonProperty("product_name_en_imported")
    private String productNameEnImported;
    @JsonProperty("product_quantity")
    private Long productQuantity;
    @JsonProperty("purchase_places")
    private String purchasePlaces;
    @JsonProperty("purchase_places_debug_tags")
    private List<String> purchasePlacesDebugTags;
    @JsonProperty("purchase_places_tags")
    private List<String> purchasePlacesTags;
    private String quantity;
    @JsonProperty("quantity_debug_tags")
    private List<String> quantityDebugTags;
    @JsonProperty("quantity_imported")
    private String quantityImported;
    @JsonProperty("removed_countries_tags")
    private List<String> removedCountriesTags;
    private Integer rev;
    @JsonProperty("scans_n")
    private Integer scansN;
    @JsonProperty("selected_images")
    private SelectedImages selectedImages;
    @JsonProperty("serving_quantity")
    private BigInteger servingQuantity;
    @JsonProperty("serving_size")
    private String servingSize;
    @JsonProperty("serving_size_debug_tags")
    private List<String> servingSizeDebugTags;
    @JsonProperty("serving_size_imported")
    private String servingSizeImported;
    private Long sortkey;
    private List<Source> sources;
    @JsonProperty("sources_fields")
    private Map<String, SourceField> sourcesFields;
    private String states;
    @JsonProperty("states_hierarchy")
    private List<String> statesHierarchy;
    @JsonProperty("states_tags")
    private List<String> statesTags;
    private String stores;
    @JsonProperty("stores_debug_tags")
    private List<String> storesDebugTags;
    @JsonProperty("stores_imported")
    private String storesImported;
    @JsonProperty("stores_tags")
    private List<String> storesTags;
    private String teams;
    @JsonProperty("teams_tags")
    private List<String> teamsTags;
    private String traces;
    @JsonProperty("traces_debug_tags")
    private List<String> tracesDebugTags;
    @JsonProperty("traces_from_ingredients")
    private String tracesFromIngredients;
    @JsonProperty("traces_from_user")
    private String tracesFromUser;
    @JsonProperty("traces_hierarchy")
    private List<String> tracesHierarchy;
    @JsonProperty("traces_imported")
    private String tracesImported;
    @JsonProperty("traces_lc")
    private String tracesLc;
    @JsonProperty("traces_tags")
    private List<String> tracesTags;
    @JsonProperty("unique_scans_n")
    private Integer uniqueScansN;
    @JsonProperty("unknown_ingredients_n")
    private Integer unknownIngredientsN;
    @JsonProperty("unknown_nutrients_tags")
    private List<String> unknownNutrientsTags;
    @JsonProperty("update_key")
    private String updateKey;
    @JsonProperty("vitamins_prev_tags")
    private List<String> vitaminsPrevTags;
    @JsonProperty("vitamins_tags")
    private List<String> vitaminsTags;
    @JsonProperty("with_sweeteners")
    private Integer withSweeteners;

    {
        LOCALIZED_PROPNAME_MAP.put("abbreviated_product_name", localizedAbbreviatedProductNames);
        LOCALIZED_PROPNAME_MAP.put("conservation_conditions", localizedConservationConditions);
        LOCALIZED_PROPNAME_MAP.put("customer_service", localizedCustomerServices);
        LOCALIZED_PROPNAME_MAP.put("generic_name", localizedGenericNames);
        LOCALIZED_PROPNAME_MAP.put("ingredients_text", localizedIngredientsTexts);
        LOCALIZED_PROPNAME_MAP.put("ingredients_text_with_allergens", localizedIngredientsTextsWithAllergens);
        LOCALIZED_PROPNAME_MAP.put("nutrition_grade", localizedNutritionGrades);
        LOCALIZED_PROPNAME_MAP.put("origin", localizedOrigins);
        LOCALIZED_PROPNAME_MAP.put("other_information", localizedOtherInformation);
        LOCALIZED_PROPNAME_MAP.put("packaging_text", localizedPackagingTexts);
        LOCALIZED_PROPNAME_MAP.put("preparation", localizedPreparations);
        LOCALIZED_PROPNAME_MAP.put("producer", localizedProducers);
        LOCALIZED_PROPNAME_MAP.put("product_name", localizedProductNames);
        LOCALIZED_PROPNAME_MAP.put("recycling_instructions_to_discard", localizedRecyclingInstructionsToDiscard);
        LOCALIZED_PROPNAME_MAP.put("recycling_instructions_to_recycle", localizedRecyclingInstructionsToRecycle);
    }

    public boolean hasEcoscoreOrGrade() {
        return anyNotNull(ecoscoreScore, ecoscoreGrade) && !equalsAny(ecoscoreGrade, "not-applicable", "unknown");
    }
}
