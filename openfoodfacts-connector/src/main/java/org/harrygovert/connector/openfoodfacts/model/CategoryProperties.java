package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class CategoryProperties extends LocalizedModel {
    private Map<String, String> localizedCiqualFoodCodes = new HashMap<>();

    {
        LOCALIZED_PROPNAME_MAP.put("ciqual_food_name", localizedCiqualFoodCodes);
    }
}
