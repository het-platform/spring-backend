package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SelectedImages extends LocalizedModel {
    private SelectedImage front;
    private SelectedImage ingredients;
    private SelectedImage nutrition;
    private SelectedImage packaging;
}
