package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class FootprintData extends LocalizedModel {
    @JsonProperty("footprint_per_kg")
    private Double footprintPerKg;
    private String grade;
    private List<FootprintDataIngredients> ingredients;
}
