package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ThreatenedSpecies extends LocalizedModel {
    private String ingredient;
    private Double value;
    private String warning;
}
