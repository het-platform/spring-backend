package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class Agribalyse extends LocalizedModel {
    @JsonProperty("agribalyse_food_code")
    private String agribalyseFoodCode;
    @JsonProperty("agribalyse_proxy_food_code")
    private String agribalyseProxyFoodCode;
    @JsonProperty("co2_agriculture")
    private Double co2Agriculture;
    @JsonProperty("co2_consumption")
    private Double co2Consumption;
    @JsonProperty("co2_distribution")
    private Double co2Distribution;
    @JsonProperty("co2_packaging")
    private Double co2Packaging;
    @JsonProperty("co2_processing")
    private Double co2Processing;
    @JsonProperty("co2_total")
    private Double co2Total;
    @JsonProperty("co2_transportation")
    private Double co2Transportation;
    private String code;
    private Double dqr;
    @JsonProperty("ef_agriculture")
    private Double efAgriculture;
    @JsonProperty("ef_consumption")
    private Double efConsumption;
    @JsonProperty("ef_distribution")
    private Double efDistribution;
    @JsonProperty("ef_packaging")
    private Double efPackaging;
    @JsonProperty("ef_processing")
    private Double efProcessing;
    @JsonProperty("ef_total")
    private Double efTotal;
    @JsonProperty("ef_transportation")
    private Double efTransportation;
    private Integer score;
    @JsonProperty("is_beverage")
    private Boolean isBeverage;
    private String warning;

    private Map<String, String> localizedNames = new HashMap<>();

    {
        LOCALIZED_PROPNAME_MAP.put("name", localizedNames);
    }
}
