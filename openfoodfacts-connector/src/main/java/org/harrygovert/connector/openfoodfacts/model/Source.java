package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class Source extends LocalizedModel {
    private List<String> fields;
    private String id;
    private List<String> images;
    @JsonProperty("import_t")
    private Integer importT;
    private String url;
    private String manufacturer;
    private String name;
    @JsonProperty("source_licence")
    private String sourceLicence;
    @JsonProperty("source_licence_url")
    private String sourceLicenceUrl;
}
