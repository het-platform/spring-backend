package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class FootprintDataIngredients extends LocalizedModel {
    @JsonProperty("conditions_tags")
    private List<List<String>> conditionsTags;
    @JsonProperty("footprint_per_kg")
    private Double footprintPerKg;
    @JsonProperty("matching_tag_id")
    private String matchingTagId;
    private Double percent;
    @JsonProperty("percent_estimate")
    private Double percentEstimate;
    @JsonProperty("processing_factor")
    private Double processing_factor;
    @JsonProperty("tag_id")
    private String tagId;
    @JsonProperty("tag_type")
    private String tagType;
}
