package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.harrygovert.connector.model.ApiError;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Builder
public class OpenFoodFactsAddProductPhotoResult extends LocalizedModel implements ApiError {
    private String code;
    private String debug;
    private String error;
    private List<FileDetails> files;
    private ImageDetails image;
    private String imagefield;
    private String imgid;
    private String status;
    @JsonProperty("status_verbose")
    private String statusVerbose;


    @Override
    public String getMessage() {
        return statusVerbose;
    }

    @Data
    @ToString
    public static class FileDetails {
        private String code;
        private String filename;
        private String name;
        private String thumbnailUrl;
        private String url;
    }

    @Data
    @ToString
    public static class ImageDetails {
        @JsonProperty("crop_url")
        private String cropUrl;
        private String imgid;
        @JsonProperty("thumb_url")
        private String thumbUrl;
    }
}
