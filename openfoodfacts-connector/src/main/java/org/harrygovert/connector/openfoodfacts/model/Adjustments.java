package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Adjustments extends LocalizedModel {
    @JsonProperty("origins_of_ingredients")
    private OriginsOfIngredients originsOfIngredients;
    private Packaging packaging;
    @JsonProperty("production_system")
    private ProductionSystem productionSystem;
    @JsonProperty("threatened_species")
    private ThreatenedSpecies threatenedSpecies;
}
