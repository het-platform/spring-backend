package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Ingredient extends LocalizedModel {
    @JsonProperty("has_sub_ingredients")
    private String hasSubIngredients;
    private String id;
    @JsonProperty("percent_estimate")
    private Double percentEstimate;
    @JsonProperty("percent_max")
    private Double percentMax;
    @JsonProperty("percent_min")
    private Double percentMin;
    private String processing;
    private Integer rank;
    private String text;
    private String vegan;
    private String vegetarian;
}
