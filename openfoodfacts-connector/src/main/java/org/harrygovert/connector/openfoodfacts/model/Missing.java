package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Missing extends LocalizedModel {
    @JsonProperty("agb_category")
    private Integer agbCategory;
    private Integer categories;
    private Integer ingredients;
    private Integer labels;
    private Integer origins;
    private Integer packagings;
}
