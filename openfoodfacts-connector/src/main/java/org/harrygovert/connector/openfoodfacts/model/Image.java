package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class Image extends LocalizedModel {
    private Integer angle;
    @JsonProperty("coordinates_image_size")
    private String coordinatesImageSize;
    private String geometry;
    private String imgid;
    private String normalize;
    private Integer ocr;
    private Integer orientation;
    private String rev;
    private Map<String, Size> sizes;
    @JsonProperty("uploaded_t")
    private Long uploadedT;
    private String uploader;
    @JsonProperty("white_magic")
    private String whiteMagic;
    private String x1;
    private String x2;
    private String y1;
    private String y2;
}
