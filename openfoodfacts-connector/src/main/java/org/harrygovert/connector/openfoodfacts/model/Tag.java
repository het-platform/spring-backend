package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Tag extends LocalizedModel {
    private String id;
    private Integer known;
    private String name;
    private Integer products;
    private String url;
}
