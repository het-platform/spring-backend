package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.ZonedDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
public class SourceField extends LocalizedModel {
    @JsonProperty("available_date")
    private String availableDate;
    @JsonProperty("fdc_category")
    private String fdcCategory;
    @JsonProperty("fdc_data_source")
    private String fdcDataSource;
    @JsonProperty("fdc_id")
    private String fdcId;
    private Long gln;
    private Long gpcCategoryCode;
    private String gpcCategoryName;
    private Boolean isAllergenRelevantDataProvided;
    private ZonedDateTime lastChangeDateTime;
    @JsonProperty("modified_date")
    private String modifiedDate;
    private String partyName;
    private String productionVariantDescription;
    @JsonProperty("publication_date")
    private String publicationDate;
    private ZonedDateTime publicationDateTime;
}
