package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class FootprintDataIngredientsType extends LocalizedModel {
    @JsonProperty("conditions_tags")
    private List<String> conditionsTags;
    @JsonProperty("deforestation_risk")
    private Double deforestationRisk;
    private String name;
    @JsonProperty("soy_feed_factor")
    private Double soyFeedFactor;
    @JsonProperty("soy_yield")
    private Double soyYield;
}
