package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class CategoriesProperties extends LocalizedModel {
    private Map<String, String> localizedAgribalyseFoodCodes = new HashMap<>();
    private Map<String, String> localizedAgribalyseProxyFoodCodes = new HashMap<>();
    private Map<String, String> localizedCiqualFoodCodes = new HashMap<>();

    {
        LOCALIZED_PROPNAME_MAP.put("agribalyse_food_code", localizedAgribalyseFoodCodes);
        LOCALIZED_PROPNAME_MAP.put("agribalyse_proxy_food_code", localizedAgribalyseProxyFoodCodes);
        LOCALIZED_PROPNAME_MAP.put("ciqual_food_code", localizedCiqualFoodCodes);
    }
}
