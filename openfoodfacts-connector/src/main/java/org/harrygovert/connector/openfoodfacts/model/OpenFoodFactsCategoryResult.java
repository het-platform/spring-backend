package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class OpenFoodFactsCategoryResult extends LocalizedModel {
    private Integer count;
    private List<Tag> tags;
}
