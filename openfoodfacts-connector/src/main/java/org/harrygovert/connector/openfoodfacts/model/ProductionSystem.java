package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class ProductionSystem extends LocalizedModel {
    private String label;
    private List<String> labels;
    private Integer value;
    private String warning;
}
