package org.harrygovert.connector.openfoodfacts.exception;

import lombok.Getter;
import lombok.NonNull;
import org.harrygovert.connector.exception.ApiException;
import org.harrygovert.connector.openfoodfacts.model.OpenFoodFactsAddProductResult;

@Getter
public class ProductAdditionFailedException extends ApiException {
    private final Integer status;
    private final String statusVerbose;

    public ProductAdditionFailedException(@NonNull OpenFoodFactsAddProductResult result) {
        super(result, result.toString());
        status = result.getStatus();
        statusVerbose = result.getStatusVerbose();
    }
}
