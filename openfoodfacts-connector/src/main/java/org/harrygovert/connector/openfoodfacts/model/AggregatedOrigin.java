package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;

@Data
public class AggregatedOrigin {
    private String origin;
    private Double percent;
}
