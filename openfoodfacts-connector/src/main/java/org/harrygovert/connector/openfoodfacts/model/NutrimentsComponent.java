package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class NutrimentsComponent extends LocalizedModel {
    private Double data;
    private String label;
    private String modifier;
    private Double per100g;
    private Double perServing;
    private String unit;
    private Double value;

    private static Double toDouble(Object object) {
        return Double.valueOf(String.valueOf(object));
    }

    public boolean setItem(String propertyName, Object propertyValue) {
        switch (propertyName) {
            case "":
                data = toDouble(propertyValue);
                return true;
            case "_100g":
                per100g = toDouble(propertyValue);
                return true;
            case "_label":
                label = String.valueOf(propertyValue);
                return true;
            case "_modifier":
                modifier = String.valueOf(propertyValue);
                return true;
            case "_serving":
                perServing = toDouble(propertyValue);
                return true;
            case "_unit":
                unit = String.valueOf(propertyValue);
                return true;
            case "_value":
                value = toDouble(propertyValue);
                return true;
            default:
                return false;
        }
    }
}
