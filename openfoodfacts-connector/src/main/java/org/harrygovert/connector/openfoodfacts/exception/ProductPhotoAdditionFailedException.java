package org.harrygovert.connector.openfoodfacts.exception;

import lombok.Getter;
import lombok.NonNull;
import org.harrygovert.connector.exception.ApiException;
import org.harrygovert.connector.openfoodfacts.model.OpenFoodFactsAddProductPhotoResult;

@Getter
public class ProductPhotoAdditionFailedException extends ApiException {
    private final String errorString;
    private final String status;
    private final String statusVerbose;

    public ProductPhotoAdditionFailedException(@NonNull OpenFoodFactsAddProductPhotoResult result) {
        super(result, result.toString());
        status = result.getStatus();
        statusVerbose = result.getStatusVerbose();
        errorString = result.getError();
    }
}
