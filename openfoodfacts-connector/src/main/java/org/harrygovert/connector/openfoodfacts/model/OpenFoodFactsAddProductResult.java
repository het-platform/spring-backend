package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.harrygovert.connector.model.ApiError;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Builder
public class OpenFoodFactsAddProductResult extends LocalizedModel implements ApiError {
    private Integer status;
    @JsonProperty("status_verbose")
    private String statusVerbose;

    @Override
    public String getMessage() {
        return statusVerbose;
    }
}
