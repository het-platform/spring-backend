package org.harrygovert.connector.openfoodfacts.model;

import lombok.Data;

@Data
public class Size {
    private Integer h;
    private Integer w;
}