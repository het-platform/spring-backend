package org.harrygovert.connector.openfoodfacts.exception;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

import static java.lang.String.format;

@Getter
@RequiredArgsConstructor
public class ProductNotFoundInOpenFoodFactsException extends RuntimeException {
    public ProductNotFoundInOpenFoodFactsException(String barcode, Integer status, String statusVerbose) {
        super(format("Product with barcode %s not found by OpenFoodFacts - got status %s: %s", barcode, status, statusVerbose));
    }
}
