package org.harrygovert.connector.openfoodfacts.model;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@EqualsAndHashCode(callSuper = true)
@Data
public class Nutriments extends LocalizedModel {
    private final Map<NutrimentComponentType, NutrimentsComponent> components = new HashMap<>() {{
        Arrays.stream(NutrimentComponentType.values())
                .forEach(type -> put(type, new NutrimentsComponent()));
    }};

    @JsonProperty("carbon-footprint-from-known-ingredients_100g")
    private Double carbonFootprintFromKnownIngredients100g;
    @JsonProperty("carbon-footprint-from-known-ingredients_product")
    private Double carbonFootprintFromKnownIngredientsProduct;
    @JsonProperty("carbon-footprint-from-known-ingredients_serving")
    private Double carbonFootprintFromKnownIngredientsServing;
    @JsonProperty("fruits-vegetables-nuts-estimate-from-ingredients_100g")
    private Double fruitsVegetablesNutsEstimateFromIngredients100g;
    @JsonProperty("nutrition-score-fr")
    private Integer nutritionScoreFr;
    @JsonProperty("nutrition-score-fr_100g")
    private Integer nutritionScoreFr100g;

    @Override
    @JsonAnySetter
    protected void setAny(String propertyName, Object propertyValue) {
        List<NutrimentComponentType> matchingTypes = components.keySet().stream()
                .filter(type -> propertyName.startsWith(type.getKeyPrefix()))
                .collect(Collectors.toList());
        for (NutrimentComponentType type : matchingTypes) {
            String propertySpecifier = propertyName.substring(type.getKeyPrefix().length());
            if (components.get(type).setItem(propertySpecifier, propertyValue)) {
                return;
            }
        }
        super.setAny(propertyName, propertyValue);
    }
}
