package org.harrygovert.connector.openfoodfacts;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.harrygovert.connector.openfoodfacts.OpenFoodFactsConnector.OpenFoodFactsSearchSpecification;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class OpenFoodFactsConnectorIT {
    private static final OpenFoodFactsConnector CONNECTOR = new OpenFoodFactsConnector(true);

    @Test
    void shouldRetrieveProductByBarcode() {
        String barcode = "3222475893421";
        var result = CONNECTOR.getProductByBarcode(barcode);
        Assertions.assertEquals(result.getCode(), barcode);
        Assertions.assertEquals(result.getProduct().getCode(), barcode);
    }

    @ParameterizedTest
    @ValueSource(strings = {"tomato-pulps", "vegetables-based-foods"})
    void shouldRetrieveProductsByCategory(String category) {
        var result = CONNECTOR.searchProductsByCategory(
                OpenFoodFactsSearchSpecification.builder().terms(category).build());
        assertNotNull(result);
        assertTrue(result.getProducts().size() <= result.getCount());
    }

    @Test
    void shouldAddProductByBarcodeAndName() {
        CONNECTOR.addProductByBarcodeAndName("8710400344452", "Test name", true);
    }
}